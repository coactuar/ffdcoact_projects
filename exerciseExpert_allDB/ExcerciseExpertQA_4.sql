-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:19 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ExcerciseExpertQA_4`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `batch` varchar(50) NOT NULL,
  `select_batch` varchar(150) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `batch`, `select_batch`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `remark`) VALUES
(1, 'batch65', 'Batch65', 'murali', '402120910', 'Phase 4 diet has more quantity of food to be taken. If I am unable to consume so much quantity what should I do?', '2021-05-24 07:25:56', '61', 0, 0, 1, 0, 1, 1, 0, 0, NULL),
(2, 'batch65', 'Batch69', 'Reena', '9045145740', 'Age 54 150 lb not losing much weight started with 156 lb. what to do', '2021-05-24 07:32:09', '1', 0, 0, 1, 0, 1, 0, 0, 0, NULL),
(3, 'batch65', 'Batch69', 'Reena', '9045145740', 'Menopause done', '2021-05-24 07:33:22', '1', 0, 0, 1, 0, 1, 0, 0, 0, NULL),
(4, 'batch65', 'Batch65', 'Kamsella chetty', '4368558669', 'Iâ€™m postmenopausal ,turned 60years 4 days ago ,61.2 kg but goes up and down .what role does hormones play in the stubborn weight around the belly ,Dr Malhar? Batch 65 phase 3 still hovering .', '2021-05-24 07:33:52', '1', 0, 0, 1, 0, 0, 1, 0, 0, NULL),
(5, 'batch65', 'Batch68', 'Tara Egan', '4088936412', 'My right leg has swollen up since yesterday, wondering what could be the reason for it?', '2021-05-24 07:33:57', '1', 0, 0, 1, 1, 0, 0, 0, 0, NULL),
(6, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'AGe 47 weight 57.5kg.  I am in athletic phase 4 biking 3 days, 2 days of yoga and one day  of strength training. I still need to lose one or two kgs to come down to BMI between 21 and 22. I am following Phase 1 diet now. Is it ok to follow this diet. I have increase my workout. I have also started glycemic ladder . Sometimes I get very hungry. what ca n I do. I dont want to stop working out', '2021-05-24 07:34:18', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(7, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'My weight is 115 Lb (52kg) 39 years. I eat my dinner by 6PM and start evening exercise at 7 ish (I am not a morning person).  My evening sugars are little higher (although I eat same food as lunch)..  Does my evening exercise contributing to sugar spike?  Is this okay?', '2021-05-24 07:34:49', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(8, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'I am from batch 64', '2021-05-24 07:35:16', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(9, 'batch65', 'Batch68', 'Tara Egan', '4088936412', 'I am at a BMI of 22.5 and how could I not gain weight in phase 3 & 4?', '2021-05-24 07:35:16', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(10, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'After going to gym what should we do first Cardio or strength training', '2021-05-24 07:36:23', '971', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(11, 'batch65', 'Batch65', 'NIRANJAN MAMILLAPALLI', '4088968086', 'Age-49, BMI-21.8. Fasting BSL is still fluctuating b/w 115-140, though avg. BSL is 128. Where does the Liver get excess sugar from, to dump it in the morning?', '2021-05-24 07:38:38', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(12, 'batch65', 'Batch68', 'Tara Egan', '4088936412', 'Should I continue with exercises despite swollen leg near the ankle?', '2021-05-24 07:38:42', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(13, 'batch65', 'Batch66', 'Shilpa Abhyankar', '39047298', 'is there diff between biking out and gym. its difficult to go out in this situation. I am 50 yrs with 62 kg. want to go on with phase4', '2021-05-24 07:40:34', '973', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(14, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'Age 39, Weight 53 kg. I have purchased spin bike (Echelon) instead of outdoor bike. I am doing it twice a week. I follow online (real-time) spin classes ~45 mins. Just wanted to know if this good enough.?\r\nI am still eating phase 2 diet ( my diet has not changed due to kidney stone related issues)', '2021-05-24 07:41:04', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(15, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'what is the maximum distance one can bike in a day at a stretch or it depends on ones strength.', '2021-05-24 07:42:12', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(16, 'batch65', 'Batch68', 'Parag', '6168209525', 'I am 45 with 63 kg weight, BMI is 24.88. My left heel pains when I get up in the morning. It started recently not sure what is the reason any exercise recommendations.', '2021-05-24 07:42:22', '1', 0, 0, 1, 0, 1, 1, 0, 0, NULL),
(17, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'can we do cardio and strength training on the same visit to gym', '2021-05-24 07:42:39', '971', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(18, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'can we do cardio and strength training on the same visit to gym', '2021-05-24 07:44:02', '971', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(19, 'batch65', 'Batch68', 'Santha Kumari Damodaran', '0133424467', 'I am so.used to running 5km in the morning and some light walk maybe 15 to 20 minutes after ...is that okay. I am on the FFD diet protocol 95% time most of time', '2021-05-24 07:44:33', '60', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(20, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'Age 39, Weight 52 kgs, BMI 23.2 . I do Yoga 3 times a week( 60 min session) , Spin bike 2 times (40 -50 min session) and 2 times weight/strength exercise (once upper body / once total body) - 30 min sessions.  Is this a good start? Also, i am having lots of body pain after weight/strength exercise..  Any tips to reduce this pain..', '2021-05-24 07:47:30', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(21, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'I am usually done with my workout by 6,30am. what effect does this have on blood sugar all day?', '2021-05-24 07:49:20', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(22, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'My current bmi is 26 and I am stuck here from last 2 months not able to loose further weight.Current weight is 60.5', '2021-05-24 07:55:28', '971', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(23, 'batch65', 'Batch69', 'Reena', '9045145740', 'Could you please go through my readings and suggest what I should do. My a1c was 6.3 when I started so the goal is to lose weight and lower bmi', '2021-05-24 07:57:31', '1', 0, 0, 1, 1, 0, 0, 0, 0, NULL),
(24, 'batch65', 'Batch65', 'murali', '402120910', 'Please give some idea about the quantity of protein diet  in a day', '2021-05-24 07:59:01', '61', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(25, 'batch65', 'Batch64', 'Shiva Swaroop ', '509693464', 'Dr.My BMI is around 22.2 and I am unable to gain weight inspite of having 4 meals a day.\r\nAlso I did BUN and Urea test the values obtained is low and Dr. told protein intake is low. I am taking regularly home made protein powder .So want to know how to increase proteins intake to gain muscle and weight?', '2021-05-24 08:01:35', '971', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(26, 'batch65', 'Batch68', 'Minal Phanse', '7632182716', 'So when start muscle building and shape training then chances are may get medicine again? so when will be medicine free after that? As the goal is GTT', '2021-05-24 08:03:59', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(27, 'batch65', 'Batch65', 'murali', '402120910', 'Your views on treadmill jogging/walking', '2021-05-24 08:08:50', '61', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(28, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'i had stopped  phase1 exercises & stair climbing since we started phase 4. just wanted to reconfirm if my understanding is correct?', '2021-05-24 08:08:57', '1', 0, 0, 0, 1, 0, 1, 0, 0, NULL),
(29, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'what % of muscle mass is good', '2021-05-24 08:08:59', '971', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(30, 'batch65', 'Batch66', 'Abhilash ', '2033938755', 'Is pea protein powder a good option', '2021-05-24 08:09:40', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(31, 'batch65', 'Batch65', 'murali', '402120910', 'Following exercise challenges videos of Dr Aditi , Sayni and Ashwini.  Are they good enough? Do i I need to join Gym?', '2021-05-24 08:11:37', '61', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(32, 'batch65', 'Batch68', 'Tara Egan', '4088936412', 'i have started phase 3 exercises, should i be doing be doing IF and water fasting ? or or do phase 3 and 4 diet?', '2021-05-24 08:14:25', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(33, 'batch65', 'Batch66', 'Sunitha Shiv', '6518902349', 'Hello Dr, need inputs on how to increase metabolic rate?', '2021-05-24 08:16:03', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(34, 'batch65', 'Batch65', 'murali', '402120910', 'I never went to Gym.  How do I approach Gym? What to ask etc..', '2021-05-24 08:16:20', '61', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(35, 'batch65', 'Batch69', 'Reena', '9045145740', 'By gym you mean Strength training with machines', '2021-05-24 08:19:02', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(36, 'batch65', 'Batch68', 'Santha Kumari Damodaran', '0133424467', 'My BMI is 23 4 I am 63, how often can I start going to gym', '2021-05-24 08:21:03', '60', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(37, 'batch65', 'Batch68', 'Tara Egan', '4088936412', 'yes', '2021-05-24 08:21:16', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(38, 'batch65', 'Batch69', 'Reena', '9045145740', 'Yes thank you', '2021-05-24 08:21:17', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(39, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'Good Session. i got my inputs', '2021-05-24 08:21:20', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(40, 'batch65', 'Batch65', 'NIRANJAN MAMILLAPALLI', '4088968086', 'Yes. Thank you very much Dr. Malhar.', '2021-05-24 08:21:25', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(41, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'ðŸ‘', '2021-05-24 08:21:37', '971', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(42, 'batch65', 'Batch65', 'Kamsella chetty', '4368558669', 'YES GRATEFUL for this gained valuable info .', '2021-05-24 08:21:51', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(43, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'is strength training at home with dumbbells and bands enough following youtube videos', '2021-05-24 08:21:57', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(44, 'batch65', 'Batch66', 'Shilpa Abhyankar', '39047298', 'yes', '2021-05-24 08:22:25', '973', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(45, 'batch65', 'Batch66', 'Luna Bhaskar', '9135687485', 'My BMI is still between 25-26, started  SNS today for next 5 days to bring it down, hopefully will be under 25 so I can start my athletic identity soon', '2021-05-24 08:22:40', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(46, 'batch65', 'Batch69', 'Reena', '9045145740', 'What is your opinion about elliptical machine', '2021-05-24 08:23:36', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(47, 'batch65', 'Batch66', 'Sunitha Shiv', '6518902349', 'Thank you, Dr! good session. This time works for USA  Pacific Time (West Coast)', '2021-05-24 08:24:01', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(48, 'batch65', 'Batch65', 'murali', '402120910', 'Thanks a lot Dr. This session is very useful. Please have this session regularly.', '2021-05-24 08:24:20', '61', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(49, 'batch65', 'Batch64', 'Prajna Shenoy', '8326576089', 'I am not going to Gym due to Covid situation.. just doing dumbel (2/3 Lbs) at home and using videos. Hope this is good enough till Covid situation comes back to normal?', '2021-05-24 08:25:53', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(50, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'Thanks for a great session Doctor. This time works good for USA central time zone.', '2021-05-24 08:27:50', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(51, 'batch65', 'Batch68', 'Santha Kumari Damodaran', '0133424467', 'This time is super....Malaysia', '2021-05-24 08:29:01', '60', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(52, 'batch65', 'Batch66', 'Luna Bhaskar', '9135687485', 'Thanks! I have started sampling, will be going for long hiking, 9 miles next weekend. \r\nThis time is perfect. Thanks again', '2021-05-24 08:30:08', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(53, 'batch65', 'Batch68', 'Santha Kumari Damodaran', '0133424467', 'Great info thank you Dr', '2021-05-24 08:30:22', '60', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(54, 'batch65', 'Batch65', 'murali', '402120910', 'Timing is good in Melbourne. Noon time. we are 4 and half hrs ahead of IST.', '2021-05-24 08:30:26', '61', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(55, 'batch65', 'Batch65', 'murali', '402120910', 'yes', '2021-05-24 08:31:15', '61', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(56, 'batch65', 'Batch69', 'Reena', '9045145740', 'Yes', '2021-05-24 08:31:16', '1', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(57, 'batch65', 'Batch64', 'Jyothi padiyar', '8476436015', 'yes', '2021-05-24 08:31:19', '1', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(58, 'batch65', 'Batch65', 'shamsunnahar', '0424206127', 'My current bmi is less 21 last 6 month n wight is 45. 5 /46. Im look like  thin cow.. Susseest how increase my wight', '2021-05-24 08:31:20', '61', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(59, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'Thanks a lot dr', '2021-05-24 08:31:27', '971', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(60, 'batch65', 'Batch66', 'Shilpa Abhyankar', '39047298', 'yes', '2021-05-24 08:31:28', '973', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(61, 'batch65', 'Batch66', 'Sunitha Shiv', '6518902349', 'Thank you so much, Dr! Love the Athelete group', '2021-05-24 08:31:31', '1', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(62, 'batch65', 'Batch68', 'Santha Kumari Damodaran', '0133424467', 'Yes', '2021-05-24 08:31:35', '60', 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `batch` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `batch`, `name`, `cntry_code`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `joining_date`) VALUES
(1, 'batch65', 'neeraj', '93', '08367773537', '2021-05-21 13:42:53', '2021-05-21 13:43:23', 1, '2021-05-21 13:42:53'),
(2, 'batch65', 'Hariom Fubey', '', '9406558700', '2021-05-22 14:54:19', '2021-05-22 14:54:49', 1, '2021-05-22 14:54:19'),
(3, 'batch65', 'Channaveerappa Hippargi', '91', '9449853005', '2021-05-22 15:10:55', '2021-05-22 15:11:25', 1, '2021-05-22 15:10:24'),
(4, 'batch65', 'ketki shah', '', '9820148242', '2021-05-23 16:30:44', '2021-05-23 16:32:55', 1, '2021-05-23 16:30:44'),
(5, 'batch65', 'Jyothi', '1', '2679942636', '2021-05-23 17:40:18', '2021-05-23 17:40:46', 0, '2021-05-23 17:40:18'),
(6, 'batch65', 'Sangeetha Krishnan', '971', '557207504', '2021-05-23 17:51:10', '2021-05-23 17:51:40', 1, '2021-05-23 17:51:10'),
(7, 'batch65', 'Kamsella chetty', '1', '4378558669', '2021-05-24 07:08:01', '2021-05-24 07:11:02', 1, '2021-05-24 06:49:31'),
(8, 'batch65', 'Nimita kalihari', '973', '35491170', '2021-05-24 07:09:51', '2021-05-24 07:10:21', 1, '2021-05-24 06:56:08'),
(9, 'batch65', 'V', '', '56', '2021-05-24 07:15:32', '2021-05-24 07:30:10', 1, '2021-05-24 06:57:43'),
(10, 'batch65', 'Kamini Sheth', '61', '404104626', '2021-05-24 07:32:51', '2021-05-24 08:31:52', 1, '2021-05-24 07:05:04'),
(11, 'batch65', 'Tara Egan', '1', '4088936412', '2021-05-24 07:17:18', '2021-05-24 09:04:49', 1, '2021-05-24 07:17:18'),
(12, '', '', '', '', '2021-05-24 07:17:48', '2021-05-24 21:43:36', 1, '2021-05-24 07:17:48'),
(13, '-1', 'Kamsella chetty', '1', '4378558669', '2021-05-24 07:19:41', '2021-05-24 07:20:11', 1, '2021-05-24 07:19:41'),
(14, 'batch65', 'Kamsella chetty', '1', '4368558669', '2021-05-24 07:20:21', '2021-05-24 08:32:01', 1, '2021-05-24 07:20:21'),
(15, 'batch65', 'NIRANJAN MAMILLAPALLI', '1', '4088968086', '2021-05-24 07:22:05', '2021-05-24 08:32:07', 1, '2021-05-24 07:22:05'),
(16, 'batch65', 'murali', '61', '402120910', '2021-05-24 07:22:28', '2021-05-24 08:38:59', 1, '2021-05-24 07:22:28'),
(17, 'batch65', 'Prajna Shenoy', '1', '8326576089', '2021-05-24 07:23:05', '2021-05-24 08:32:06', 1, '2021-05-24 07:23:05'),
(18, 'batch65', 'Reena', '1', '9045145740', '2021-05-24 07:25:47', '2021-05-24 08:31:49', 1, '2021-05-24 07:25:47'),
(19, 'batch65', 'Yogini pevekar', '1', '8482190480', '2021-05-24 07:27:12', '2021-05-24 08:32:45', 1, '2021-05-24 07:27:12'),
(20, 'batch65', 'shobha sekhar', '61', '0426257441', '2021-05-24 07:28:39', '2021-05-24 09:18:20', 1, '2021-05-24 07:28:39'),
(21, 'batch65', 'Jyothi padiyar', '1', '8476436015', '2021-05-24 07:29:07', '2021-05-24 08:31:38', 0, '2021-05-24 07:29:07'),
(22, 'batch65', 'Shilpa Abhyankar', '973', '39047298', '2021-05-24 07:31:12', '2021-05-24 08:32:14', 1, '2021-05-24 07:31:12'),
(23, 'batch65', 'Shubhada Pradhan', '1', '4088593436', '2021-05-24 07:31:55', '2021-05-24 08:31:57', 1, '2021-05-24 07:31:55'),
(24, 'batch65', 'Santha Kumari Damodaran', '60', '0133424467', '2021-05-24 08:10:12', '2021-05-24 08:31:43', 1, '2021-05-24 07:33:20'),
(25, 'batch65', 'Ninni Mattoo', '44', '7980363610', '2021-05-24 07:33:28', '2021-05-24 07:35:30', 1, '2021-05-24 07:33:28'),
(26, 'batch65', 'Minal Phanse', '1', '7632182716', '2021-05-24 07:33:38', '2021-05-24 08:32:10', 1, '2021-05-24 07:33:38'),
(27, 'batch65', 'Parul kasliwal', '971', '506828063', '2021-05-24 07:34:00', '2021-05-24 08:32:01', 1, '2021-05-24 07:34:00'),
(28, 'batch65', 'Parag', '1', '6168209525', '2021-05-24 07:35:56', '2021-05-24 08:08:27', 0, '2021-05-24 07:35:56'),
(29, 'batch65', 'Nutan iyer', '1', '7133808089', '2021-05-24 07:36:53', '2021-05-24 08:33:40', 1, '2021-05-24 07:36:53'),
(30, 'batch65', 'R Acharya', '65', '98635349', '2021-05-24 07:38:58', '2021-05-24 07:39:58', 1, '2021-05-24 07:38:58'),
(31, 'batch65', 'Rupa Kango', '1', '8654560422', '2021-05-24 07:45:57', '2021-05-24 08:31:59', 1, '2021-05-24 07:45:57'),
(32, 'batch65', 'Shiva Swaroop ', '971', '509693464', '2021-05-24 07:47:55', '2021-05-24 08:08:28', 1, '2021-05-24 07:47:55'),
(33, 'batch65', 'Jacintha', '91', '8087075057', '2021-05-24 07:49:11', '2021-05-24 08:09:25', 0, '2021-05-24 07:49:11'),
(34, 'batch65', 'shamsunnahar', '61', '0424206127', '2021-05-24 07:51:11', '2021-05-24 08:37:47', 1, '2021-05-24 07:51:11'),
(35, 'batch65', 'Gayathri ', '1', '8476125174', '2021-05-24 07:53:40', '2021-05-24 07:58:41', 1, '2021-05-24 07:53:40'),
(36, 'batch65', 'Abhilash ', '1', '2033938755', '2021-05-24 07:59:34', '2021-05-24 08:33:36', 1, '2021-05-24 07:59:34'),
(37, 'batch65', 'Lynette Mendes', '1', '6479271136', '2021-05-24 08:04:49', '2021-05-24 08:32:22', 1, '2021-05-24 08:04:49'),
(38, 'batch65', 'Mona', '1', '8583369454', '2021-05-24 08:07:42', '2021-05-24 08:38:24', 1, '2021-05-24 08:07:42'),
(39, 'batch65', 'Luna Bhaskar', '1', '9135687485', '2021-05-24 08:09:05', '2021-05-24 08:31:46', 1, '2021-05-24 08:09:05'),
(40, 'batch65', 'Sunitha Shiv', '1', '6518902349', '2021-05-24 08:09:28', '2021-05-24 11:03:04', 1, '2021-05-24 08:09:28'),
(41, 'batch65', 'Ashwini Doddihal ', '971', '543244074', '2021-05-24 08:14:56', '2021-05-24 08:32:28', 1, '2021-05-24 08:14:56'),
(42, 'batch65', 'Snehalata', '91', '9975937875', '2021-05-24 08:23:13', '2021-05-24 08:35:05', 1, '2021-05-24 08:23:13'),
(43, 'batch65', 'Rupa', '1', '4697671122', '2021-05-24 08:26:51', '2021-05-24 08:31:53', 1, '2021-05-24 08:26:51'),
(44, 'batch65', 'Satish', '62', '81586845980', '2021-05-24 08:45:40', '2021-05-24 08:52:11', 1, '2021-05-24 08:42:54'),
(45, 'batch65', 'Mousumi Ranaut ', '974', '66872746', '2021-05-24 09:35:07', '2021-05-24 09:40:40', 1, '2021-05-24 09:35:07'),
(46, 'batch65', 'Dilip Mysore', '61', '448481748', '2021-05-24 10:35:25', '2021-05-24 10:37:27', 1, '2021-05-24 10:35:25'),
(47, 'batch65', 'Seema Upadhye', '971', '551570539', '2021-05-24 14:32:09', '2021-05-24 14:32:39', 1, '2021-05-24 14:32:09'),
(48, 'batch65', 'Iffat Javed', '1', '4439308916', '2021-05-24 18:44:42', '2021-05-24 18:45:12', 1, '2021-05-24 18:44:42'),
(49, 'batch65', 'Dilip Mysore', '61', '0448481748', '2021-05-24 19:06:09', '2021-05-24 19:06:39', 1, '2021-05-24 19:06:09'),
(50, 'batch65', 'Anuradha Patil ', '1', '4045186761', '2021-05-24 19:16:01', '2021-05-24 19:16:31', 1, '2021-05-24 19:16:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
