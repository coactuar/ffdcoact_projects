-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:30 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ExcerciseExpertQA_16`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `batch` varchar(50) NOT NULL,
  `select_batch` varchar(150) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `InnerTransformation` int(11) DEFAULT '0',
  `Sales` int(11) DEFAULT '0',
  `Program` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `batch`, `select_batch`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `InnerTransformation`, `Sales`, `Program`, `remark`) VALUES
(1, 'batch65', 'Batch69', 'v', '9765875731', 'Hii', '2021-11-15 12:05:40', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(2, 'batch65', 'Batch65', 'Harsha Shetty', '0505686940', 'My PP1 sometimes is on a higher side though my breakfast is as recommended by FFD.  Its not always.  L\r\nDoes lack of sleep effect PP1.    I do sometime have problems with my sleep .  Am not been able sleep for 6 to 7 hours sometimes.', '2021-11-17 17:10:21', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Diabetic Body Clock - when your body dumps liver sugar'),
(3, 'batch65', 'Batch72', 'Vijay Chauhan', '8950256637', 'Doctor has prescribed 3-2-1 exercise regime.  Can I do more also? I mean, more yoga, more walking on certain days.\r\nWhy there is no change in weight and why BSL is also not stabilizing? \r\nI feel too tired after doing exercises? I donâ€™t feel energetic', '2021-11-21 18:37:02', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(4, 'batch65', 'Batch66', 'Akshay Sawant', '7743613430', 'Hello Doc', '2021-11-21 18:59:32', '44', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(5, 'batch65', 'Batch71', 'Sekhar Chandrasekhar', '9087976290', 'Hello Dr. Malhar ... thanks for your valuable guidance during these sessions ..   Interesting onvervation,  most times, 2 glasses of red wine keeps FSL below 100 but a little bit of fruits bumps up FSL significantly .. any reason?', '2021-11-21 19:01:19', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(6, 'batch65', 'Batch65', 'Pablita Khanna', '6029033774', 'Hi Dr. Ganla, during some exercises i feel click-click movements in my elbow or shoulder. Should I keep doing the exercises in the hope that those clicks will stop, or by doing the exercises it will worsen something?', '2021-11-21 19:02:18', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'for me.....'),
(7, 'batch65', 'Batch73', 'reena shah', '9045145740', 'good morning 73', '2021-11-21 19:02:51', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(8, 'batch65', 'Batch68', 'Dewanand Gharde', '85186769', 'Hi Doc, With Similar food and routine my BSL  PP2 & PP# is fluctuating, Fasting and PP1 is good. Why so....', '2021-11-21 19:03:21', '65', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Body clock'),
(9, 'batch65', 'Batch71', 'Mala mandayam', '15103641836', 'Is better to exercise in the evening or in the morning', '2021-11-21 19:03:39', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(10, 'batch65', 'Batch73', 'Geetha Iyer', '5108097474', 'Batch 73.  My PPs go like 240 and then come down to 140.  I am on no meds.  It is not coming down.  Sometimes it is 150.', '2021-11-21 19:05:11', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Carb trigger - qualitative'),
(11, 'batch65', 'Batch66', 'Akshay Sawant', '7743613430', 'Recently read something about fasting and building strength. Do you think exercising straight after fast would work for Diabetics? Will be eating well after exercise. Looking to challenge myself to exercise without eating beforehand.', '2021-11-21 19:05:15', '44', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(12, 'batch65', 'Batch66', 'Seema Upadhye', '0551570539', 'My foot end side gives pain and my foot fingures causes lot of stretch , when sleep', '2021-11-21 19:07:02', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(13, 'batch65', 'Batch73', 'Sunita Pandya', '7322883157', 'My son is 19 , now his suger under control he has borderline per dibites, lost weight more , cause of diet , he is college going boy . Just guide me how he LL gain his weight and body built', '2021-11-21 19:09:01', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(14, 'batch65', 'Batch72', 'Krishnan Subramanian', '4124274500', 'How would one know their fat deposit in liver is used', '2021-11-21 19:13:23', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(15, 'batch65', 'Batch65', 'Harsha Shetty', '0505686940', 'Am ok with current setup', '2021-11-21 19:14:05', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(16, 'batch65', 'Batch68', 'Dewanand Gharde', '85186769', 'we can join Zoom, if it is more engaging', '2021-11-21 19:14:17', '65', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(17, 'batch65', 'Batch68', 'Dewanand Gharde', '85186769', '65-73', '2021-11-21 19:15:37', '65', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(18, 'batch65', 'Batch65', 'Pablita Khanna', '6029033774', 'When climbing stairs, my right ankle has begun to \"buckle\"/give way. Should I keep doing stairs while holding on to a railing, or should i let the ankle rest for several days before trying again? I always get confused when to keep doing exercises to overcome pain, versus when to rest to not aggravate things.', '2021-11-21 19:15:41', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(19, 'batch65', 'Batch68', 'Dewanand Gharde', '85186769', 'Are you sending Link on Whats app', '2021-11-21 19:16:08', '65', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(20, 'batch65', 'Batch66', 'Seema Upadhye', '0551570539', 'Where is the link , i am 66 batch', '2021-11-21 19:16:20', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(21, 'batch65', 'Batch73', 'Sunita Pandya', '7322883157', 'My son is 19 years old he detected borderline sugar now within 1 month he has control all ppt and weight lost more , now need to weigh gain or bold a body shape Pl guide', '2021-11-21 19:17:03', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(22, 'batch65', 'Batch66', 'Seema Upadhye', '0551570539', 'Joined', '2021-11-21 19:18:21', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(23, 'batch65', 'Batch73', 'Geetha Iyer', '5108097474', 'Could you pls show the meeting passcode again?', '2021-11-21 19:20:24', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(24, 'batch65', 'Batch71', 'Sekhar Chandrasekhar', '9087976290', 'Dr Malhar ... may be a mere coincidence,  after starting regular 3-2-1 (\r\nincluding exercise bands and weights) and sticking to it meticulously,  i find my floaters in the eye increasing....  any correlation?  or coincidence?', '2021-11-21 19:31:08', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(25, 'batch65', 'Batch71', 'Rupali Gupta ', '60430461', 'Not audible to me', '2021-11-21 19:32:34', '965', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(26, 'batch65', 'Batch66', 'Akshay Sawant', '7743613430', 'Doc, these calls give  lot of motivation. thanks for taking the time.', '2021-11-21 19:52:10', '44', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(27, 'batch65', 'Batch68', 'Satishchandra Rao Philar ', '9880414559', 'Video has stopped. So I am logging out.', '2021-11-21 19:55:20', '91', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(28, 'batch65', 'Batch71', 'Daphne', '4166682927', 'I gave fallen completely off track. Need help to get back', '2021-11-21 20:05:04', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `batch` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `batch`, `name`, `cntry_code`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `joining_date`) VALUES
(1, 'batch65', 'v', '', '9765875731', '2021-12-01 14:41:14', '2021-12-01 14:44:59', 0, '2021-11-08 14:55:37'),
(2, 'batch65', 'Harsha Shetty', '971', '0505686940', '2021-11-21 19:16:29', '2021-11-21 19:19:23', 1, '2021-11-17 17:06:28'),
(3, 'batch65', 'Geetha Iyer', '1', '5108097474', '2021-11-21 19:00:44', '2021-11-21 19:30:06', 1, '2021-11-18 19:17:35'),
(4, 'batch65', 'Rani Cheema', '1', '7149651758', '2021-11-19 09:11:12', '2021-11-19 09:17:24', 1, '2021-11-19 09:11:12'),
(5, 'batch65', 'Sanjay', '61', '0426267203', '2021-11-19 17:40:09', '2021-11-19 17:40:39', 1, '2021-11-19 17:40:09'),
(6, 'batch65', 'Pragna p Modha ', '1', '8923020', '2021-11-21 17:54:06', '2021-11-21 17:54:36', 1, '2021-11-19 19:28:45'),
(7, 'batch65', 'Pramila Sanjaya', '', '9829215633', '2021-11-20 17:21:28', '2021-11-20 17:22:29', 1, '2021-11-20 17:21:28'),
(8, 'batch65', 'Vincent', '65', '97947642', '2021-11-20 17:23:10', '2021-11-20 17:23:40', 1, '2021-11-20 17:23:10'),
(9, 'batch65', 'Nandita Jain', '33', '0633320306', '2021-11-21 19:15:13', '2021-11-21 19:19:14', 1, '2021-11-20 18:55:54'),
(10, 'batch65', 'Kamsella', '1', '4378558669', '2021-11-21 18:53:31', '2021-11-21 19:55:07', 0, '2021-11-20 19:28:41'),
(11, 'batch65', 'Iffat Javed', '1', '4439308916', '2021-11-21 18:50:26', '2021-11-21 20:00:22', 1, '2021-11-21 18:02:31'),
(12, 'batch65', 'Vijay Chauhan', '', '8950256637', '2021-11-21 19:00:10', '2021-11-21 19:20:11', 1, '2021-11-21 18:28:06'),
(13, 'batch65', 'Biju Kaimal', '971', '505593010', '2021-11-21 18:46:19', '2021-11-21 20:18:38', 1, '2021-11-21 18:46:19'),
(14, 'batch65', 'JP', '1', '2245587015', '2021-11-21 18:52:10', '2021-11-21 20:38:41', 1, '2021-11-21 18:52:10'),
(15, 'batch65', 'MK', '1', '7702657506', '2021-11-21 19:54:56', '2021-11-21 20:01:58', 1, '2021-11-21 18:52:20'),
(16, 'batch65', 'V', '', '546', '2021-11-21 18:53:01', '2021-11-21 18:53:31', 1, '2021-11-21 18:53:01'),
(17, 'batch65', 'Deepak Kumar Mishra', '971', '566133146', '2021-11-21 18:57:22', '2021-11-21 19:54:22', 1, '2021-11-21 18:57:22'),
(18, 'batch65', 'Raja Rajeswara Rao Velpucharla', '', '9160111520', '2021-11-21 18:58:08', '2021-11-21 19:20:17', 0, '2021-11-21 18:58:08'),
(19, 'batch65', 'Monika Bhola', '', '9814109129', '2021-11-21 18:58:10', '2021-11-21 21:30:31', 1, '2021-11-21 18:58:10'),
(20, 'batch65', 'Dewanand Gharde', '65', '85186769', '2021-11-21 18:58:42', '2021-11-21 19:19:42', 1, '2021-11-21 18:58:42'),
(21, 'batch65', 'Sekhar Chandrasekhar', '1', '9087976290', '2021-11-21 18:58:54', '2021-11-21 20:22:27', 1, '2021-11-21 18:58:54'),
(22, 'batch65', 'Krishnan Subramanian', '1', '4124274500', '2021-11-21 19:04:36', '2021-11-21 19:15:52', 1, '2021-11-21 18:58:55'),
(23, 'batch65', 'Akshay Sawant', '44', '7743613430', '2021-11-21 19:40:52', '2021-11-21 20:02:07', 1, '2021-11-21 18:58:56'),
(24, 'batch65', 'Santha Kumari Damodaran', '60', '133424467', '2021-11-21 19:02:05', '2021-11-21 19:55:06', 1, '2021-11-21 18:59:04'),
(25, 'batch65', 'Ranganath ', '1', '2815087931', '2021-11-21 18:59:43', '2021-11-21 19:54:15', 1, '2021-11-21 18:59:43'),
(26, 'batch65', 'Pablita Khanna', '1', '6029033774', '2021-11-21 18:59:58', '2021-11-21 19:19:59', 1, '2021-11-21 18:59:58'),
(27, 'batch65', 'Pragna Modha ', '1', '3198923020', '2021-11-21 19:11:15', '2021-11-21 19:25:27', 1, '2021-11-21 19:00:07'),
(28, 'batch65', 'Mala mandayam', '1', '15103641836', '2021-11-21 19:00:46', '2021-11-21 19:19:54', 1, '2021-11-21 19:00:46'),
(29, 'batch65', 'Seema Upadhye', '971', '0551570539', '2021-11-21 19:15:52', '2021-11-21 19:36:47', 1, '2021-11-21 19:00:51'),
(30, 'batch65', 'reena shah', '1', '9045145740', '2021-11-21 19:01:15', '2021-11-21 19:21:17', 1, '2021-11-21 19:01:15'),
(31, 'batch65', 'Tara egan', '1', '4088936412', '2021-11-21 19:05:07', '2021-11-21 20:09:39', 1, '2021-11-21 19:03:06'),
(32, 'batch65', 'Neeraja', '1', '6093696908', '2021-11-21 19:04:13', '2021-11-21 19:55:22', 1, '2021-11-21 19:04:13'),
(33, 'batch65', 'SHERLY RAJAN ', '968', '92448545', '2021-11-21 19:04:27', '2021-11-21 19:39:58', 1, '2021-11-21 19:04:27'),
(34, 'batch65', 'Sunita Pandya', '1', '7322883157', '2021-11-21 19:05:23', '2021-11-21 19:18:59', 1, '2021-11-21 19:05:23'),
(35, 'batch65', 'Luna Bhaskar', '1', '9135687485', '2021-11-21 19:05:45', '2021-11-21 19:19:51', 1, '2021-11-21 19:05:45'),
(36, 'batch65', 'Radhika narasimhan', '1', '2065503632', '2021-11-21 21:20:10', '2021-11-21 21:34:39', 1, '2021-11-21 19:08:31'),
(37, 'batch65', 'Satish', '62', '81586845980', '2021-11-21 19:15:49', '2021-11-21 19:59:59', 1, '2021-11-21 19:13:38'),
(38, 'batch65', 'TIM SUNDER', '1', '7744513824', '2021-11-21 19:14:06', '2021-11-21 19:57:39', 1, '2021-11-21 19:14:06'),
(39, 'batch65', 'Abhilash', '1', '2033938755', '2021-11-21 19:57:55', '2021-11-21 19:58:25', 1, '2021-11-21 19:16:40'),
(40, 'batch65', 'Elizabeth ', '971', '506451545', '2021-11-21 19:18:30', '2021-11-21 19:24:01', 1, '2021-11-21 19:18:30'),
(41, 'batch65', 'Atul', '1', '7326924593', '2021-11-21 19:20:48', '2021-11-21 19:53:50', 1, '2021-11-21 19:20:48'),
(42, 'batch65', 'Simi gupta', '1', '7324073684', '2021-11-21 19:21:59', '2021-11-21 19:45:32', 1, '2021-11-21 19:21:59'),
(43, 'batch65', 'Manoj pevekar', '1', '8484593779', '2021-11-21 19:23:38', '2021-11-21 20:01:05', 1, '2021-11-21 19:23:38'),
(44, 'batch65', 'Suparna Aggarwal', '1', '2487600092', '2021-11-21 19:24:15', '2021-11-21 19:56:59', 1, '2021-11-21 19:24:15'),
(45, 'batch65', 'Rupali Gupta ', '965', '60430461', '2021-11-21 19:58:59', '2021-11-21 19:59:29', 1, '2021-11-21 19:29:21'),
(46, 'batch65', 'seetha Jayaraman', '1', '4168312080', '2021-11-21 19:32:32', '2021-11-21 19:50:41', 1, '2021-11-21 19:32:32'),
(47, 'batch65', 'Jayaprakash ', '965', '99593517', '2021-11-21 19:37:09', '2021-11-21 19:38:12', 1, '2021-11-21 19:37:09'),
(48, 'batch65', 'Vinay Tiwari', '1', '9252160436', '2021-11-21 19:41:58', '2021-11-21 19:44:33', 1, '2021-11-21 19:41:58'),
(49, 'batch65', 'Rupa', '1', '4697671122', '2021-11-21 19:45:09', '2021-11-21 19:53:41', 1, '2021-11-21 19:45:09'),
(50, 'batch65', 'Yogendra', '230', '52585577', '2021-11-21 20:02:18', '2021-11-21 20:03:49', 1, '2021-11-21 19:46:04'),
(51, 'batch65', 'Sunanda kanshi', '1', '6465955376', '2021-11-21 19:56:12', '2021-11-21 19:58:13', 1, '2021-11-21 19:46:26'),
(52, 'batch65', 'Hema Srinivas', '1', '2402710037', '2021-11-21 19:47:16', '2021-11-21 19:58:16', 1, '2021-11-21 19:47:16'),
(53, 'batch65', 'Satishchandra Rao Philar ', '91', '9880414559', '2021-11-21 19:53:52', '2021-11-21 19:55:53', 1, '2021-11-21 19:50:57'),
(54, 'batch65', 'Maria Picardo ', '44', '07404593049', '2021-11-21 19:57:13', '2021-11-21 20:14:35', 1, '2021-11-21 19:57:13'),
(55, 'batch65', 'sreelata M Pillai', '1', '7032090476', '2021-11-21 19:57:13', '2021-11-21 20:13:52', 1, '2021-11-21 19:57:13'),
(56, 'batch65', 'Radhika natasimhan', '1', '065503632', '2021-11-21 20:00:48', '2021-11-21 20:04:09', 1, '2021-11-21 20:00:48'),
(57, 'batch65', 'Daphne', '1', '4166682927', '2021-11-21 20:04:11', '2021-11-21 21:36:49', 1, '2021-11-21 20:04:11'),
(58, '-1', 'Lalitha balakrishnan', '44', '7368909658', '2021-11-21 20:06:02', '2021-11-21 20:06:32', 1, '2021-11-21 20:05:04'),
(59, 'batch65', 'Lalitha balakrishnan', '44', '7368909658', '2021-11-21 20:07:13', '2021-11-21 20:08:44', 1, '2021-11-21 20:07:13'),
(60, 'batch65', 'Lalit Nahata', '977', '9851051881', '2021-11-21 20:30:34', '2021-11-21 20:31:04', 1, '2021-11-21 20:30:34'),
(61, 'batch65', 'Nima', '1', '5025261866', '2021-11-21 21:48:03', '2021-11-21 21:48:33', 1, '2021-11-21 21:48:03'),
(62, 'batch65', 'chetana walvekar', '', '9845546265', '2021-11-22 07:21:25', '2021-11-22 07:21:55', 1, '2021-11-22 07:21:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
