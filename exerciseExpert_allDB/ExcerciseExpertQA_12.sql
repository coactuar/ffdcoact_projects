-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:29 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ExcerciseExpertQA_12`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `batch` varchar(50) NOT NULL,
  `select_batch` varchar(150) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `InnerTransformation` int(11) DEFAULT '0',
  `Sales` int(11) DEFAULT '0',
  `Program` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `batch`, `select_batch`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `InnerTransformation`, `Sales`, `Program`, `remark`) VALUES
(1, 'batch65', 'Batch71', 'vibhav', '9765875731', 'Hii', '2021-09-23 11:01:43', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(2, 'batch65', 'Batch71', 'vibhav', '9765875731', 'Hii', '2021-09-23 11:01:51', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(3, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'Dr. Malhar: Have lost 12 lbs since the start of my FFD batch 70. However I need to go down by another 10 lbs to reach my goal of 23 BMI. However I am stuck at 180 lbs. After starting g Phase 4, I started getting more hungry and put on 4 lbs and thus skewed my downward trend of weight loss. What should I do. Should I concentrate on Phase 3 and start Phase 4 later?', '2021-09-26 17:40:13', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(4, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'Dr. Malhar: In he Phase 4 Formula 3:2; 1, can I have one of them as HIIT? \r\nPlease be informed I am 69 years of Age. Is HIIT OK for this Age? I fi nd cycling and AGE exercises very conducive. also, studies after studies have shown that brisk walking is veery beneficial. However I find you have downplayed it a bit, is there a particular reason?', '2021-09-26 17:43:09', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(5, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'Dr Malhar: what is the central idea of developing an athletic identity? Is it to form a strong self-image?', '2021-09-26 17:44:24', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(6, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'How should the diet change with Phase 4 given the fact we are still at the Phase 2/Phase 3 diet of Juices, smoothies, sprouts etc etc etc, Plesse throw some light. are eggs OK?', '2021-09-26 17:49:01', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(7, 'batch65', 'Batch71', 'SEKHAR CHANDRASEKHAR', '9087976290', 'Batch 71...  My BMI 21 ... Have experienced 3 Kgs weight loss since start of program.  Stopped green smoothie.   Following all diet and exercise guidelines.  Doing resistance band for muscle build up.  Is 3Kg loss normal??', '2021-09-26 18:30:29', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(8, 'batch65', 'Batch70', 'Vandana Vora', '4035104869', 'Hello Dr. Malhar. I am enjoying my 3:2:1 4th phase. Weight and core training, running and yoga. Can i run on treadmill because weather not favourable- fall and soon winter. My after dinner BSL is high what will you suggest regarding exercise to help BSL. Morning is the best time for me to do my training. Afternoon walk. Thank you Sir NamasteðŸ™ðŸ™ðŸ¤', '2021-09-26 18:36:49', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(9, 'batch65', 'Batch64', 'Snehalata ', '9975937875', 'What to do to make bones stronger', '2021-09-26 18:37:11', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(10, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'does sugar holding capacity or more carb holding capacity increase with increase in strength training', '2021-09-26 18:46:26', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(11, 'batch65', 'Batch70', 'Vandana Vora', '4035104869', 'Yes', '2021-09-26 19:01:11', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(12, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'yes', '2021-09-26 19:01:16', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(13, 'batch65', 'Batch71', 'SWAMINATHAN V', '562161233', 'YES', '2021-09-26 19:01:24', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(14, 'batch65', 'Batch65', 'Kamsella', '4378558669', 'Yes', '2021-09-26 19:01:26', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(15, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'Yes', '2021-09-26 19:01:34', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(16, 'batch65', 'Batch70', 'Vandana Vora', '4035104869', 'Both are goodðŸ‘ðŸ‘', '2021-09-26 19:01:39', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(17, 'batch65', 'Batch70', 'Vandana Vora', '4035104869', '70', '2021-09-26 19:01:52', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(18, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', 'yes\r\n70', '2021-09-26 19:01:55', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(19, 'batch65', 'Batch68', 'Saji Oommen', '506267357', '68', '2021-09-26 19:02:01', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(20, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', '70', '2021-09-26 19:02:01', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(21, 'batch65', 'Batch64', 'JP', '2245587015', '64', '2021-09-26 19:02:05', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(22, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', '65', '2021-09-26 19:02:06', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(23, 'batch65', 'Batch70', 'Vandana Vora', '4035104869', '70', '2021-09-26 19:02:09', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(24, 'batch65', 'Batch70', 'Nayan Joshi', '4045104450', '70', '2021-09-26 19:02:09', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(25, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', '65', '2021-09-26 19:02:12', '94', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(26, 'batch65', 'Batch64', 'Jumri', '7653609707', '64', '2021-09-26 19:02:20', '44', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(27, 'batch65', 'Batch68', 'Saji Oommen', '506267357', '68', '2021-09-26 19:02:21', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(28, 'batch65', 'Batch68', 'Saji Oommen', '506267357', '68', '2021-09-26 19:02:34', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(29, 'batch65', 'Batch65', 'Kamsella', '4378558669', '65 international', '2021-09-26 19:02:37', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(30, 'batch65', 'Batch68', 'Saji Oommen', '506267357', '68', '2021-09-26 19:02:50', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(31, 'batch65', 'Batch68', 'Saji Oommen', '506267357', '68', '2021-09-26 19:04:00', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(32, 'batch65', 'Batch64', 'JP', '2245587015', '64', '2021-09-26 19:04:07', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(33, 'batch65', 'Batch71', 'Saras Shekar', '8483914081', 'If we miss out on certain diet or exercises or inner transformation due to travel can we start from where we left?', '2021-09-26 19:04:13', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(34, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'Please share it on Diabtethletes', '2021-09-26 19:05:10', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(35, 'batch65', 'Batch65', 'Kamsella', '4378558669', 'Share in diabetheltics 11 whatâ€™s app Dr Malhar ?', '2021-09-26 19:05:25', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(36, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'Batch 68 not added', '2021-09-26 19:06:06', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '846 2408 7136     Password: 165823'),
(37, 'batch65', 'Batch71', 'Sekhar Chandrasekhar', '9082684020', 'r u going to continue here?  or go to zoom?', '2021-09-26 19:08:17', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(38, 'batch65', 'Batch66', 'Neeta Seshmukj', '4083960964', 'Can you please put zoom link on WhatsApp also', '2021-09-26 19:08:36', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(39, 'batch65', 'Batch68', 'sreelata Pillai', '7032090476', 'Are we changing to zoom', '2021-09-26 19:09:14', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(40, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', 'Along with my 3:2:1 Exercises I do Nitric Oxide Dump 2 hours after meal to avoid taking medicine - I need to know whether the effect of Nitric Oxide Dump lasts for at least 4 to 5 hours or is it only momentary for that moment only.', '2021-09-26 19:12:37', '94', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(41, 'batch65', 'Batch68', 'sreelata Pillai', '7032090476', 'Hello Dr Malhar i am not able to lose weight. Is there anything i can do . I am still on medications and have not completely come out of it', '2021-09-26 19:14:02', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(42, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', 'what is the average time duration for the weights exercise in minutes per day ....if I am doing weights 5 days per week.', '2021-09-26 19:17:08', '94', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(43, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'I have reduced my BMI from 26 to 23.5 now.  Can I target for 25?', '2021-09-26 19:17:20', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(44, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'I currently focus on Yoga and my BSL looks good without medicines. Can I continue with Yoga forgetting 3-2-1 protocol?', '2021-09-26 19:20:49', '971', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(45, 'batch65', 'Batch71', 'SHERLY RAJAN', '92448545', 'This is Sherly Rajan. Good Evening. my problem  I am doing my daily routine plus my work... I feel very tied . My BMI is right now23.5.(It was 25.9 ).if I increase food it will increase sugar my level.  Then I have to go for medication . what  I have to do', '2021-09-26 19:21:26', '968', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(46, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', 'I read that Muscle repair takes place during Deep Sleep (non REM Sleep) and that Melatonin will assist in deep sleep. I took Melatonin but my FBS started shooting up - I checked the internet and found that  Melatonin is supposed to supress Insulin .....\r\nI am in a dliema on whether to continue Melatonin and use Glycomet to induce Insulin  - I am currently free of medication until I started Melatonin.....', '2021-09-26 19:22:20', '94', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(47, 'batch65', 'Batch68', 'Saji Oommen', '506267357', 'All recommended tips to reduce FSL failed in my case. However, my PP1,2,3 are  much in control. Any suggestions?', '2021-09-26 19:27:09', '971', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(48, 'batch65', 'Batch71', 'M. Jayasudha', '91524160', '71 international\r\n\r\nDear Dr. Always bsl 2 is high any advise?', '2021-09-26 19:31:09', '65', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(49, 'batch65', 'Batch67', 'Raghu', '9736102860', 'Been diabetic for 20 years and got rid of medicines since the 5 months. My BMI went from 28 to 24. But it is stuck at 24 for the past 3 months. Can I start phase 4?', '2021-09-26 19:32:40', '1', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(50, 'batch65', 'Batch64', 'Jyothi Padiyar', '8476436015', 'Biking 25 to 30 miles once in a week or 10 miles each on 3 days . Which one is better?', '2021-09-26 19:37:45', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(51, 'batch65', 'Batch71', 'Atul', '7326924593', 'Is it ok to have egg whites in phase 4?', '2021-09-26 19:39:51', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(52, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', 'I do treadmill at an incline at low speed with a weighted jacket .....is this ok ...\r\nI do it daily prior to going to bed ....cos I do some religious recitations for 30 mts and I thought of doing the recitations while on the treadmill rather than seated...', '2021-09-26 19:40:49', '94', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(53, 'batch65', 'Batch66', 'Rupa', '4697671122', 'Of the 7, 11 and 10 pm, pm what uncooked food can you eat at 10 pm', '2021-09-26 19:42:53', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(54, 'batch65', 'Batch71', 'Atul', '7326924593', 'I m injured with rib contusion. Is there any guidance on specific food for faster recovery?', '2021-09-26 19:47:43', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(55, 'batch65', 'Batch71', 'Sekhar Chandrasekhar', '9082684020', 'My Doctor has put me on Reclide 60mg ER and Taurin.  How long can I have this medications?  It has helped my BSL, my glucometer says 60 day average is 103.', '2021-09-26 19:55:24', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(56, 'batch65', 'Batch68', 'sreelata Pillai', '7032090476', 'Doctor what is plato which you keep mentioning', '2021-09-26 19:57:47', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(57, 'batch65', 'Batch71', 'Atul', '7326924593', 'Hi doc \r\nI have raised couple of questions \r\n\r\nCan u plz answer?', '2021-09-26 20:08:02', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(58, 'batch65', 'Batch71', 'M. Jayasudha', '91524160', 'So true excellent section Sir. Thank you so much.', '2021-09-26 20:29:24', '65', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(59, 'batch65', 'Batch65', 'Mohammed Mumthaz Ansar', '779955515', 'Feedback .....the zoom questions ....takes a whole lot of time ....', '2021-09-26 20:37:41', '94', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(60, 'batch65', 'Batch68', 'Suparna Aggarwal', '2487600092', 'Thanks a lot ðŸ™ðŸ¼ðŸ™ðŸ¼', '2021-09-26 20:40:44', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(61, 'batch65', 'Batch67', 'Mahek', '9723667807', 'Sir how should I take a appointment to talk with you? You told to take a appointment with you.', '2021-10-03 20:32:54', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(62, 'batch65', 'Batch67', 'Mahek', '9723667807', 'Sir how should I take a appointment with you you told to take a appointment with you??', '2021-10-03 20:39:02', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(63, 'batch65', 'Batch67', 'Mahek', '9723667807', 'By by ðŸ™thank you for this session', '2021-10-03 20:41:02', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `batch` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `batch`, `name`, `cntry_code`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `joining_date`) VALUES
(1, 'batch65', 'vibhav', '', '9765875731', '2021-09-23 11:01:35', '2021-09-23 11:01:54', 0, '2021-09-23 11:01:35'),
(2, 'batch65', 'Priti Pungalia ', '', '8810525351', '2021-09-23 22:48:34', '2021-09-23 22:49:04', 1, '2021-09-23 22:48:34'),
(3, 'batch65', 'Sadanand Byakod ', '', '6360755002', '2021-09-25 11:59:20', '2021-09-25 12:01:52', 1, '2021-09-25 11:59:20'),
(4, 'batch65', 'Ismat Jahangir', '1', '7038998389', '2021-09-26 19:03:22', '2021-09-26 20:41:24', 1, '2021-09-25 19:01:11'),
(5, 'batch65', 'Saikumar Venkataraman Kenya', '254', '723387833', '2021-09-25 19:02:26', '2021-09-25 19:49:26', 1, '2021-09-25 19:02:26'),
(6, 'batch65', 'bhagat singh panwar', '965', '97287837', '2021-09-25 20:39:02', '2021-09-25 20:39:32', 1, '2021-09-25 20:39:02'),
(7, 'batch65', 'Lata Khubchandani', '91', '09820028499', '2021-09-26 14:10:38', '2021-09-26 14:11:08', 1, '2021-09-26 14:10:38'),
(8, 'batch65', 'Nishanth', '93', '5643', '2021-09-26 14:20:23', '2021-09-26 15:09:26', 0, '2021-09-26 14:20:23'),
(9, 'batch65', 'Nayan Joshi', '1', '4045104450', '2021-09-26 18:48:48', '2021-09-26 19:06:52', 1, '2021-09-26 17:36:03'),
(10, 'batch65', 'sreelata Pillai', '1', '7032090476', '2021-09-26 19:02:29', '2021-09-26 20:41:23', 1, '2021-09-26 17:50:24'),
(11, 'batch65', 'Krishnan', '1', '7328419378', '2021-09-26 19:07:45', '2021-09-26 20:29:47', 1, '2021-09-26 17:59:59'),
(12, 'batch65', 'SEKHAR CHANDRASEKHAR', '1', '9087976290', '2021-09-26 19:01:53', '2021-09-26 19:02:54', 1, '2021-09-26 18:27:29'),
(13, 'batch65', 'Nishanth', '355', '56464', '2021-09-26 18:27:33', '2021-09-26 18:41:09', 0, '2021-09-26 18:27:33'),
(14, 'batch65', 'Akshay Jain', '973', '38018813', '2021-09-26 18:28:14', '2021-09-26 18:28:44', 1, '2021-09-26 18:28:14'),
(15, 'batch65', 'Vandana Vora', '1', '4035104869', '2021-09-26 18:30:32', '2021-09-26 19:03:10', 1, '2021-09-26 18:30:32'),
(16, 'batch65', 'Snehalata ', '', '9975937875', '2021-09-26 18:35:00', '2021-09-26 19:10:26', 1, '2021-09-26 18:35:00'),
(17, 'batch65', 'Parul kasliwal', '971', '506828063', '2021-09-26 18:54:46', '2021-09-26 19:08:47', 1, '2021-09-26 18:37:29'),
(18, 'batch65', 'Sekhar Chandrasekhar', '1', '9082684020', '2021-09-26 18:39:54', '2021-09-26 20:40:56', 1, '2021-09-26 18:39:54'),
(19, 'batch65', 'Nishanth', '93', '51233', '2021-09-26 18:41:37', '2021-09-26 19:00:45', 0, '2021-09-26 18:41:37'),
(20, 'batch65', 'Tara Egan', '1', '4088936412', '2021-09-26 18:51:20', '2021-09-26 20:46:22', 1, '2021-09-26 18:51:20'),
(21, 'batch65', 'Deepak Kumar Mishra', '971', '566133146', '2021-09-26 18:51:36', '2021-09-26 20:24:37', 1, '2021-09-26 18:51:36'),
(22, 'batch65', 'Kamsella', '1', '4378558669', '2021-09-26 18:52:12', '2021-09-26 19:09:52', 0, '2021-09-26 18:52:12'),
(23, 'batch65', 'Sheela saharajan ', '973', '39769060', '2021-09-26 18:52:59', '2021-09-26 20:34:37', 1, '2021-09-26 18:52:26'),
(24, '-1', 'Iyer', '1', '7133808089', '2021-09-26 18:55:01', '2021-09-26 18:55:31', 1, '2021-09-26 18:55:01'),
(25, 'batch65', 'Prabhakar Thummala', '1', '2083409442', '2021-09-26 18:55:40', '2021-09-26 19:12:42', 1, '2021-09-26 18:55:40'),
(26, 'batch65', 'JP', '1', '2245587015', '2021-09-26 18:56:12', '2021-09-26 19:14:13', 1, '2021-09-26 18:56:12'),
(27, 'batch65', 'Uzzal Hazarika', '968', '95225041', '2021-09-26 18:56:20', '2021-09-26 19:14:57', 1, '2021-09-26 18:56:20'),
(28, 'batch65', 'Noel Hopkins', '1', '6474006635', '2021-09-26 18:56:27', '2021-09-26 20:53:29', 1, '2021-09-26 18:56:27'),
(29, 'batch65', 'Anup Wagadre', '44', '07760137674', '2021-09-26 18:56:36', '2021-09-26 19:45:54', 0, '2021-09-26 18:56:36'),
(30, 'batch65', 'M. Jayasudha', '65', '91524160', '2021-09-26 18:56:57', '2021-09-26 20:40:28', 1, '2021-09-26 18:56:57'),
(31, 'batch65', 'Hitendra Badrakiya', '', '9824310906', '2021-09-26 18:57:17', '2021-09-26 20:25:38', 0, '2021-09-26 18:57:17'),
(32, 'batch65', 'Nutan iyer', '1', '7133808089', '2021-09-26 18:57:21', '2021-09-26 19:13:22', 1, '2021-09-26 18:57:21'),
(33, 'batch65', 'Suparna Aggarwal', '1', '2487600092', '2021-09-26 19:11:14', '2021-09-26 20:41:17', 1, '2021-09-26 18:57:39'),
(34, 'batch65', 'Yogini pevekar', '1', '8484593779', '2021-09-26 18:58:49', '2021-09-26 20:41:51', 1, '2021-09-26 18:58:49'),
(35, 'batch65', 'SWAMINATHAN V', '971', '562161233', '2021-09-26 18:59:30', '2021-09-26 19:05:01', 1, '2021-09-26 18:59:30'),
(36, 'batch65', 'Saji Oommen', '971', '506267357', '2021-09-26 19:00:29', '2021-09-26 20:40:42', 1, '2021-09-26 18:59:54'),
(37, 'batch65', 'Mohammed Mumthaz Ansar', '94', '779955515', '2021-09-26 19:00:31', '2021-09-26 20:46:40', 1, '2021-09-26 19:00:31'),
(38, 'batch65', 'Saras Shekar', '1', '8483914081', '2021-09-26 19:45:32', '2021-09-26 20:12:35', 1, '2021-09-26 19:01:21'),
(39, 'batch65', 'Jumri', '44', '7653609707', '2021-09-26 19:01:24', '2021-09-26 19:08:29', 1, '2021-09-26 19:01:24'),
(40, 'batch65', 'Atul', '1', '7326924593', '2021-09-26 19:39:02', '2021-09-26 20:18:21', 1, '2021-09-26 19:02:00'),
(41, 'batch65', 'Neeta Seshmukj', '1', '4083960964', '2021-09-26 19:02:04', '2021-09-26 19:18:07', 1, '2021-09-26 19:02:04'),
(42, 'batch65', 'Raghavendra Murthy', '1', '9736102760', '2021-09-26 19:03:00', '2021-09-26 19:07:02', 1, '2021-09-26 19:03:00'),
(43, 'batch65', 'Luna Bhaskar', '1', '9135687485', '2021-09-26 19:03:19', '2021-09-26 19:13:39', 1, '2021-09-26 19:03:19'),
(44, 'batch65', 'Hem Nandan', '353', '877576698', '2021-09-26 19:06:50', '2021-09-26 19:09:21', 1, '2021-09-26 19:03:19'),
(45, 'batch65', 'SHERLY RAJAN', '968', '92448545', '2021-09-26 19:03:26', '2021-09-26 20:41:05', 1, '2021-09-26 19:03:26'),
(46, 'batch65', 'R Acharya', '65', '98635349', '2021-09-26 19:03:35', '2021-09-26 20:41:06', 1, '2021-09-26 19:03:35'),
(47, 'batch65', 'Elizabeth Mascarenhas ', '971', '506451545', '2021-09-26 19:04:04', '2021-09-26 19:07:40', 1, '2021-09-26 19:04:04'),
(48, 'batch65', 'Lynette', '1', '6479271136', '2021-09-26 19:04:49', '2021-09-26 20:40:51', 1, '2021-09-26 19:04:49'),
(49, 'batch65', 'Tim Sunder', '1', '7744513824', '2021-09-26 19:05:31', '2021-09-26 19:13:33', 1, '2021-09-26 19:05:31'),
(50, 'batch65', 'Rupali Gupta ', '965', '60430461', '2021-09-26 19:06:37', '2021-09-26 20:39:10', 1, '2021-09-26 19:06:37'),
(51, '-1', 'Krishnan', '1', '7328419378', '2021-09-26 19:07:06', '2021-09-26 19:07:36', 1, '2021-09-26 19:07:06'),
(52, 'batch65', 'Sabagir Hussain', '966', '559927365', '2021-09-26 19:07:16', '2021-09-26 19:12:18', 1, '2021-09-26 19:07:16'),
(53, 'batch65', 'Sandhya Rao', '1', '9012390905', '2021-09-26 19:07:49', '2021-09-26 20:40:51', 1, '2021-09-26 19:07:49'),
(54, 'batch65', 'Rupa', '1', '4697671122', '2021-09-26 19:12:58', '2021-09-26 20:09:34', 0, '2021-09-26 19:12:58'),
(55, 'batch65', 'Uma', '65', '94742202', '2021-09-26 19:16:32', '2021-09-26 20:44:32', 1, '2021-09-26 19:16:32'),
(56, 'batch65', 'Sandhya Pratap', '', '9821213041', '2021-09-26 19:18:33', '2021-09-26 20:41:07', 1, '2021-09-26 19:17:17'),
(57, 'batch65', 'Raghu', '1', '9736102860', '2021-09-26 19:30:58', '2021-09-26 19:46:19', 1, '2021-09-26 19:30:58'),
(58, 'batch65', 'Jyothi Padiyar', '1', '8476436015', '2021-09-26 19:36:25', '2021-09-26 20:38:00', 1, '2021-09-26 19:36:25'),
(59, 'batch65', 'Dipak', '1', '8134078628', '2021-09-26 19:52:29', '2021-09-26 19:52:59', 1, '2021-09-26 19:52:29'),
(60, 'batch65', 'Sallauddin Attar', '91', '9975739452', '2021-09-26 20:08:06', '2021-09-26 20:34:07', 1, '2021-09-26 20:08:06'),
(61, 'batch65', 'Mona', '1', '8583369454', '2021-10-14 05:17:02', '2021-10-14 05:17:32', 1, '2021-09-26 20:22:26'),
(62, 'batch65', 'Daphne Britto', '1', '4166682927', '2021-09-26 21:36:10', '2021-09-26 21:40:46', 1, '2021-09-26 21:36:10'),
(63, 'batch65', 'Sabu basanther ', '', '860686', '2021-09-27 20:20:08', '2021-09-27 20:20:31', 0, '2021-09-27 20:20:08'),
(64, 'batch65', 'Sabu basanther ', '', '8606838049', '2021-09-27 20:20:49', '2021-09-27 20:31:49', 0, '2021-09-27 20:20:49'),
(65, 'batch65', 'Anupam Das Gupta', '91', '9926905433', '2021-09-30 09:57:26', '2021-09-30 09:57:56', 1, '2021-09-30 09:57:26'),
(66, 'batch65', 'Poonam anantwar', '', '7021778652', '2021-10-03 09:56:17', '2021-10-03 09:56:47', 1, '2021-10-03 09:56:17'),
(67, 'batch65', 'Mahek', '', '9723667807', '2021-10-03 20:31:15', '2021-10-03 20:41:47', 1, '2021-10-03 18:51:51'),
(68, 'batch65', 'Maria Picardo ', '44', '7404593049', '2021-10-03 21:34:38', '2021-10-03 21:37:40', 1, '2021-10-03 21:34:38'),
(69, 'batch65', 'Urmil Bhatt', '', '91982527607', '2021-10-05 15:59:06', '2021-10-05 16:09:09', 1, '2021-10-05 15:59:06'),
(70, 'batch65', 'Gorak', '91', '9840201545', '2021-10-09 08:25:18', '2021-10-09 08:25:48', 1, '2021-10-09 08:25:18'),
(71, 'batch65', 'Subhash', '', '7019246455', '2021-10-13 06:48:32', '2021-10-13 06:49:02', 1, '2021-10-13 06:48:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
