-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:29 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ExcerciseExpertQA_7`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `batch` varchar(50) NOT NULL,
  `select_batch` varchar(150) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `batch`, `select_batch`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `remark`) VALUES
(1, 'batch65', 'Batch63', 'Vibhav', '25346', 'Hii How r u?', '2021-07-19 12:38:03', '', 0, 0, 1, 0, 0, 0, 0, 0, 'DOing well'),
(2, 'batch65', 'Batch66', 'Vibhav', '25346', 'Hii How r u?', '2021-07-19 12:38:14', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(3, 'batch65', 'Batch68', 'v', '56', 'dgfd', '2021-07-19 15:37:03', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(4, 'batch65', 'Batch66', 'v', '56', 'rtet', '2021-07-19 15:37:14', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(5, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'Can we build muscle and loss weight/fat in the same phase', '2021-07-25 18:50:41', '971', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(6, 'batch65', 'Batch68', 'Hema Srinivas', '2402710037', 'Dr Malhar , how are you? i enjoy listening to you to your valuable advice . Utahns you. I have been very consistent of following diet and following exercise protocols , my journey seems to be be very slow, I have been Diabetic foe over 27 years. my medicines have been reduced to minimum, and I have lost weight in the past four months, I believe my stressor is the fact my sugars fluctuate,  how do I manage that?  Can I get off my. Medications completely? Help!!', '2021-07-25 18:53:01', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(7, 'batch65', 'Batch69', 'Vijendra Murthy', '9734419637', 'I am at the cusp of BMI 23. Should I start Phase 4? I understand Phase 4 for batch 69 started 2 weeks ago. How do I catchup? Also, I have developed severe knee problems. What 3,2,1 can you suggest for people like me?', '2021-07-25 19:00:16', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(8, 'batch65', 'Batch68', 'Swapna Chigullapalli', '4074010356', 'How to reduce fasting glucose?', '2021-07-25 19:01:17', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(9, 'batch65', 'Batch64', 'JP', '2245587015', 'Give us some details on carb cycling', '2021-07-25 19:01:17', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(10, 'batch65', 'Batch68', 'Swapna Chigullapalli', '4074010356', 'If I have high avg 125 fasting sugar levels  and others PP is low than normal avg 109.. will I still pass GTT?', '2021-07-25 19:06:15', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(11, 'batch65', 'Batch68', 'VINAY PARADKAR', '97207748', 'the assigned doctor writes to us but others ???', '2021-07-25 19:06:53', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(12, 'batch65', 'Batch69', 'Vijendra Murthy', '9734419637', 'The live stream went offline just as you were answering my question and came back after. Could you please repeat the answer - about my knee problem and what 3,2,1 can I do?', '2021-07-25 19:07:05', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(13, 'batch65', 'Batch68', 'Padma', '96278625', 'Video got cut ..can you please give details on carb cycling', '2021-07-25 19:08:04', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(14, 'batch65', 'Batch64', 'Mousumi Ranaut', '66872746', 'Glycemic ladder wk 2 \r\nI ate 2 bananas. My pp1 is 94\r\nIs there something wrong', '2021-07-25 19:10:01', '974', 0, 0, 1, 0, 1, 0, 0, 0, NULL),
(15, 'batch65', 'Batch66', 'Abhilash', '2023938755', 'How do we ensure that we are not burning muscle mass while cycling', '2021-07-25 19:10:05', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(16, 'batch65', 'Batch68', 'VINAY PARADKAR', '97207748', 'my normal sugar is in line with the requirement ( 1 meal a day in the evening ) , but having 2 chapatis of khapli atta once a day ( in the evaning )  increases the  sugar above 150. is it ok ?', '2021-07-25 19:11:56', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(17, 'batch65', 'Batch68', 'RK', '2815087931', 'Can I do yoga and muscle in the morning and cycling in the evening. What are positive and negative', '2021-07-25 19:11:58', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(18, 'batch65', 'Batch62', 'Agoston', '8681970', 'In my experience the speed of glucose absorption and insulin secretion after eating different foods is individual. Somehow we should make our own individual map by systematically checking. Whatâ€™s your opinion?', '2021-07-25 19:15:04', '36', 0, 0, 1, 0, 0, 0, 0, 0, 'AGE - Advanced Glycated end products'),
(19, 'batch65', 'Batch64', 'G V Rao', '0504238233', 'yes', '2021-07-25 19:15:16', '971', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(20, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'Can you explain carb cycling', '2021-07-25 19:16:02', '971', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(21, 'batch65', 'Batch67', 'Hasmukhbhai J Patel', '5868725190', 'Unable to increase BMI above 18.5, no matter how much i eat or rest.', '2021-07-25 19:17:33', '1', 0, 0, 0, 0, 1, 0, 0, 0, 'Ectopic fat'),
(22, 'batch65', 'Batch68', 'Padma', '96278625', 'how to reduce body fat', '2021-07-25 19:19:55', '65', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(23, 'batch65', 'Batch68', 'VINAY PARADKAR', '97207748', 'written earlier.......required to reduce weight from 86 kg to 63 kg ( based on required BMI ) . The first 12 kg got reduced in 3 months easily having alkaline diet. The next every kg is a problem ! even after following everything related to diet recommended & 75% of exercise recommended.', '2021-07-25 19:20:30', '65', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(24, 'batch65', 'Batch65', 'Parul kasliwal', '506828063', 'Doing fast for 1 day week will affect muscle massâ€¦?', '2021-07-25 19:26:28', '971', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(25, 'batch65', 'Batch68', 'Jyothi Arvind', '9788440730', 'Hello Doctor: I am getting mixed guidance. When I had a consult with you, you said I should move to phase 4. However when I had a consult with my doctor, she asked me to reduce 4 more kg and I am back to phase 2. I am a little lost. I am unable to loose weight though. I am 58.4 kg - 24.3 BMI - down from 28.94', '2021-07-25 19:27:14', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(26, 'batch65', 'Batch68', 'Jyothi Arvind', '9788440730', 'Hello Doctor: I am getting mixed guidance. When I had a consult with you, you said I should move to phase 4. However when I had a consult with my doctor, she asked me to reduce 4 more kg and I am back to phase 2. I am a little lost. I am unable to loose weight though. I am 58.4 kg - 24.3 BMI - down from 28.94', '2021-07-25 19:27:15', '1', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(27, 'batch65', 'Batch66', 'Abhilash', '2023938755', 'What is the protein powder you would suggest', '2021-07-25 19:29:16', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(28, 'batch65', 'Batch65', 'Mahesh Rayapoodi', '559195971', 'BMI is 23.4 , HBA1C is 6 with medicine . Doing same exercise protocol 3-2-1.How can I measure carbs and other ingredients?', '2021-07-25 19:31:44', '971', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(29, 'batch65', 'Batch65', 'Anjali R Deshmukh', '4084258813', 'what is the relation of gluten content to glycemic index', '2021-07-25 19:38:05', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(30, 'batch65', 'Batch68', 'Padma', '96278625', 'Doing 3-2-1 , 3 is swimming. How much swimming should we do - in terms of intensity or time?', '2021-07-25 19:38:49', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(31, 'batch65', 'Batch64', 'Mousumi Ranaut', '66872746', 'Are you going to share carb cycling pdf on app', '2021-07-25 19:39:47', '974', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(32, 'batch65', 'Batch62', 'Agoston', '8681970', 'I have tested all grain types. Each shoot my bs even in a small portion. May I have pulse /nut flours instead (Type1child)?', '2021-07-25 19:40:43', '36', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(33, 'batch65', 'Batch68', 'Padma', '96278625', 'Able to do most excercise challenges. But findig it difficult to do Vrichasam (standing on one leg). Any tips or help? Thanks', '2021-07-25 19:41:10', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(34, 'batch65', 'Batch64', 'Hitendra Badrakiya', '9824310906', 'If I heard correctly, you were to share something for low weight participants to gain weight.', '2021-07-25 19:44:37', '91', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(35, 'batch65', 'Batch68', 'Kavitha', '9132440297', 'For how long we have to continue vegan diet or is it for life?', '2021-07-25 19:45:14', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(36, 'batch65', 'Batch64', 'Hitendra Badrakiya', '9824310906', 'What is nutmeg? Can you please explain.', '2021-07-25 19:47:03', '91', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(37, 'batch65', 'Batch68', 'Padma', '96278625', 'during vegan keto should we have vegan fat more', '2021-07-25 19:49:26', '65', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(38, 'batch65', 'Batch65', 'Mahesh Rayapoodi', '559195971', 'what is uncooked meal ?', '2021-07-25 19:51:07', '971', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(39, 'batch65', 'Batch68', 'Kavitha', '9132440297', 'Iâ€™m kind of confused, Iâ€™m hearing that I can experiment with food this at this stage, including milk? But dieticians said no to milk', '2021-07-25 19:51:08', '1', 0, 0, 1, 0, 0, 1, 0, 0, 'WICOS - Who is captain of the ship?'),
(40, 'batch65', 'Batch62', 'Agoston', '8681970', 'How to deal with hunger? Especially after meals? My kid is hungry when in hyperglycemia and near hypo as well. When he is hungry he is very nervous. I canâ€™t make a good decision even if I prick him. When he is hungry he wants to have something into his mouth. Whatâ€™s your advice?', '2021-07-25 19:52:15', '36', 0, 0, 1, 0, 0, 0, 0, 0, 'Normoglycemia - Satiety'),
(41, 'batch65', 'Batch68', 'Ramanan', '96683774', 'Bmi 21, hba1c of 5.1\r\nPresently taking about 50 grams  carb. Doing okay.\r\nStarted 3-2-1. Is this meal healthy to be followed throughout life.', '2021-07-25 19:59:40', '65', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(42, 'batch65', 'Batch65', 'VENKATA CHALLA', '3362477583', 'Please brief on 3-2-1 ( Is it related to Diet or exercise) ? Sorry for the basic question', '2021-07-25 19:59:53', '1', 0, 0, 1, 0, 0, 1, 0, 0, NULL),
(43, 'batch65', 'Batch68', 'Ramanan', '96683774', '59 grams of carb per meal', '2021-07-25 20:01:13', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(44, 'batch65', 'Batch62', 'Agoston', '8681970', 'Can type1s survive on uncooked meal? We had a long cut in electricity so the idea came to me that some emergency diet plans would be needed to have at hand for cases like that or for sudden traveling etc.', '2021-07-25 20:01:27', '36', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(45, 'batch65', 'Batch64', 'G V Rao', '0504238233', 'Good session. On a lighter side. you said we cant be a Roger Federer, I like it.  By the end of this program of 1 year, can we reach some where close to  be a Junior Federer at least', '2021-07-25 20:03:22', '971', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(46, 'batch65', 'Batch62', 'Agoston', '8681970', 'Thanks!', '2021-07-25 20:03:34', '36', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(47, 'batch65', 'Batch61', 'Shruti Kulkarni ', '2679928516', 'Thank you so much! Very motivating session!', '2021-07-25 20:03:54', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(48, 'batch65', 'Batch64', 'Mousumi Ranaut', '66872746', 'My 14 yr daughter has slightly elevated testosteroneâ€¦what to do', '2021-07-25 20:05:13', '974', 0, 0, 1, 0, 0, 0, 0, 0, 'VOMIT?... '),
(49, 'batch65', 'Batch69', 'Umesh Kumar Vohra', '5300697465', 'not reached BMI goal but can i work on 3:2:!?', '2021-07-25 20:05:17', '90', 0, 0, 1, 0, 1, 1, 0, 0, NULL),
(50, 'batch65', 'Batch69', 'Umesh Kumar Vohra', '5300697465', 'Thank You!', '2021-07-25 20:09:19', '90', 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `batch` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `batch`, `name`, `cntry_code`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `joining_date`) VALUES
(1, 'batch65', 'Vibhav', '', '25346', '2021-07-19 12:37:44', '2021-07-19 12:39:07', 1, '2021-07-19 12:37:44'),
(2, 'batch65', 'Archana', '91', '7304882444', '2021-07-19 15:14:53', '2021-07-19 15:15:23', 1, '2021-07-19 15:14:53'),
(3, 'batch65', 'V', '', '5666', '2021-07-19 15:35:15', '2021-07-19 15:36:46', 1, '2021-07-19 15:35:15'),
(4, 'batch65', 'Rajeev Srivastava', '91', '7985586737', '2021-07-21 07:20:04', '2021-07-21 07:24:35', 1, '2021-07-21 07:20:04'),
(5, 'batch65', 'Nishanth', '93', '16464', '2021-07-23 14:46:33', '2021-07-23 14:49:48', 0, '2021-07-23 14:46:33'),
(6, 'batch65', 'shamsunnahar  Islam', '61', '0424206127', '2021-07-25 16:45:11', '2021-07-25 16:45:41', 1, '2021-07-23 16:14:57'),
(7, 'batch65', 'Prabu Arumugam', '', '9901491013', '2021-07-23 19:17:29', '2021-07-23 19:17:59', 1, '2021-07-23 19:17:29'),
(8, 'batch65', 'Nishanth', '', '69461', '2021-07-24 07:08:21', '2021-07-24 07:08:31', 0, '2021-07-24 07:08:21'),
(9, 'batch65', 'Sangeeta vamburkar', '1', '7327638714', '2021-07-24 18:18:01', '2021-07-24 21:35:58', 1, '2021-07-24 18:18:01'),
(10, 'batch65', 'Shylaja Nathan', '1', '9788094789', '2021-07-24 18:38:17', '2021-07-24 18:38:47', 1, '2021-07-24 18:38:17'),
(11, 'batch65', 'VINAY PARADKAR', '65', '97207748', '2021-07-25 19:05:31', '2021-07-25 19:52:02', 1, '2021-07-24 18:59:35'),
(12, 'batch65', 'R Acharya', '65', '98635349', '2021-07-24 19:09:05', '2021-07-24 19:09:35', 1, '2021-07-24 19:09:05'),
(13, 'batch65', 'V', '', '654', '2021-07-25 16:42:52', '2021-07-25 17:48:36', 1, '2021-07-25 16:42:52'),
(14, 'batch65', 'Maria Picardo ', '44', '07404593049', '2021-07-25 19:59:37', '2021-07-25 20:02:08', 1, '2021-07-25 18:21:54'),
(15, 'batch65', 'Swapna Chigullapalli', '1', '4074010356', '2021-07-25 18:58:59', '2021-07-25 20:10:09', 1, '2021-07-25 18:27:15'),
(16, 'batch65', 'Sopan Shinde ', '971', '563988804', '2021-07-25 19:05:25', '2021-07-25 20:11:27', 1, '2021-07-25 18:34:44'),
(17, 'batch65', 'V', '', '6465', '2021-07-25 18:38:43', '2021-07-25 18:41:17', 1, '2021-07-25 18:38:43'),
(18, 'batch65', 'Hema Srinivas', '1', '2402710037', '2021-07-25 18:43:18', '2021-07-25 20:10:19', 1, '2021-07-25 18:43:18'),
(19, '-1', 'Nutan iyer', '1', '7133808089', '2021-07-25 18:46:55', '2021-07-25 18:47:25', 1, '2021-07-25 18:46:55'),
(20, 'batch65', 'Nutan iyer', '1', '7133808089', '2021-07-25 18:47:47', '2021-07-25 20:31:11', 1, '2021-07-25 18:47:47'),
(21, 'batch65', 'Parul kasliwal', '971', '506828063', '2021-07-25 18:48:51', '2021-07-25 20:10:42', 1, '2021-07-25 18:48:51'),
(22, 'batch65', 'NITIN BIDIKAR', '', '9440685113', '2021-07-25 18:50:14', '2021-07-25 20:16:20', 1, '2021-07-25 18:50:14'),
(23, 'batch65', 'Jumri', '44', '7753609707', '2021-07-25 18:51:55', '2021-07-25 20:09:56', 1, '2021-07-25 18:51:55'),
(24, 'batch65', 'Bandana', '971', '557579456', '2021-07-25 19:33:01', '2021-07-25 19:34:53', 1, '2021-07-25 18:52:09'),
(25, 'batch65', 'Ramanan', '65', '96683774', '2021-07-25 19:56:21', '2021-07-25 20:17:15', 1, '2021-07-25 18:53:39'),
(26, 'batch65', 'Sita Ghate', '1', '5104575470', '2021-07-25 18:55:11', '2021-07-25 20:11:42', 1, '2021-07-25 18:55:11'),
(27, 'batch65', 'Mousumi Ranaut', '974', '66872746', '2021-07-25 18:56:11', '2021-07-25 20:10:32', 1, '2021-07-25 18:56:11'),
(28, 'batch65', 'Hasmukhbhai J Patel', '1', '5868725190', '2021-07-25 19:03:13', '2021-07-25 20:13:52', 1, '2021-07-25 18:56:14'),
(29, 'batch65', 'Tara Egan', '1', '4088936412', '2021-07-25 18:57:09', '2021-07-25 20:13:11', 1, '2021-07-25 18:57:09'),
(30, 'batch65', 'Shruti Kulkarni ', '1', '2679928516', '2021-07-25 19:09:24', '2021-07-25 20:09:55', 1, '2021-07-25 18:57:26'),
(31, 'batch65', 'Leela', '60', '122951759', '2021-07-25 18:58:00', '2021-07-25 20:09:50', 1, '2021-07-25 18:58:00'),
(32, 'batch65', 'Vijendra Murthy', '1', '9734419637', '2021-07-25 18:58:12', '2021-07-25 20:09:43', 1, '2021-07-25 18:58:12'),
(33, 'batch65', 'VENKATA CHALLA', '1', '3362477583', '2021-07-25 18:58:42', '2021-07-25 20:09:39', 0, '2021-07-25 18:58:42'),
(34, 'batch65', 'JP', '1', '2245587015', '2021-07-25 18:59:30', '2021-07-25 20:05:04', 1, '2021-07-25 18:59:30'),
(35, 'batch65', 'Hitendra Badrakiya', '91', '9824310906', '2021-07-25 19:05:35', '2021-07-25 20:09:30', 0, '2021-07-25 18:59:33'),
(36, 'batch65', 'Archana Uppal', '1', '2035438052', '2021-07-25 19:00:01', '2021-07-25 20:04:15', 1, '2021-07-25 19:00:01'),
(37, 'batch65', 'Kalpana Manikantan ', '1', '7039966043', '2021-07-25 19:01:49', '2021-07-25 20:05:20', 1, '2021-07-25 19:01:49'),
(38, 'batch65', 'Padma', '65', '96278625', '2021-07-25 19:05:52', '2021-07-25 21:07:36', 1, '2021-07-25 19:01:53'),
(39, 'batch65', 'Hanumanth Rao Bhounsle', '65', '96654803', '2021-07-25 19:01:54', '2021-07-25 20:03:54', 1, '2021-07-25 19:01:54'),
(40, 'batch65', 'Mona', '1', '8583369454', '2021-07-25 19:02:08', '2021-07-25 20:28:55', 1, '2021-07-25 19:02:08'),
(41, 'batch65', 'Yogini pevekar', '1', '8484593779', '2021-07-25 19:03:40', '2021-07-25 20:10:12', 1, '2021-07-25 19:03:40'),
(42, 'batch65', 'Jyothi Arvind', '1', '9788440730', '2021-07-25 19:46:00', '2021-07-25 20:33:31', 1, '2021-07-25 19:03:49'),
(43, 'batch65', 'Agoston', '36', '8681970', '2021-07-25 19:55:59', '2021-07-25 20:10:40', 1, '2021-07-25 19:04:53'),
(44, 'batch65', 'Mahesh Rayapoodi', '971', '559195971', '2021-07-25 19:05:02', '2021-07-25 20:21:47', 1, '2021-07-25 19:05:02'),
(45, 'batch65', 'Ramakrishnan venkataraman', '974', '55087083', '2021-07-25 19:05:29', '2021-07-25 20:09:30', 1, '2021-07-25 19:05:29'),
(46, 'batch65', 'G V Rao', '971', '0504238233', '2021-07-25 19:06:06', '2021-07-25 20:10:08', 1, '2021-07-25 19:06:06'),
(47, 'batch65', 'Abhilash', '1', '2023938755', '2021-07-25 19:06:55', '2021-07-25 20:10:00', 1, '2021-07-25 19:06:55'),
(48, 'batch65', 'RK', '1', '2815087931', '2021-07-25 19:37:12', '2021-07-25 20:10:12', 1, '2021-07-25 19:07:11'),
(49, 'batch65', 'Kavita Dhuri', '974', '70018177', '2021-07-25 19:07:17', '2021-07-25 20:10:18', 1, '2021-07-25 19:07:17'),
(50, 'batch65', 'Archana Balasubramanian', '1', '2064122724', '2021-07-25 19:08:20', '2021-07-25 20:10:55', 1, '2021-07-25 19:08:20'),
(51, 'batch65', 'Shylaja Raman ', '65', '97416250', '2021-07-25 19:08:56', '2021-07-25 19:36:56', 1, '2021-07-25 19:08:56'),
(52, 'batch65', 'Ashwini Doddihal ', '971', '543244074', '2021-07-25 19:08:56', '2021-07-25 20:39:30', 1, '2021-07-25 19:08:56'),
(53, 'batch65', 'suparna Aggarwal', '1', '2487600092', '2021-07-25 19:09:27', '2021-07-25 20:09:58', 1, '2021-07-25 19:09:27'),
(54, 'batch65', 'Sandhya Pratap', '91', '9821213941', '2021-07-25 19:09:32', '2021-07-25 20:15:07', 1, '2021-07-25 19:09:32'),
(55, 'batch65', 'Satish R Pansare', '62', '08158684598', '2021-07-25 19:12:50', '2021-07-25 20:10:27', 1, '2021-07-25 19:12:50'),
(56, 'batch65', 'Anjali R Deshmukh', '1', '4084258813', '2021-07-25 19:13:57', '2021-07-25 20:05:29', 1, '2021-07-25 19:13:57'),
(57, 'batch65', 'Jayanthi', '1', '6096516098', '2021-07-25 20:03:12', '2021-07-25 20:09:43', 1, '2021-07-25 19:19:33'),
(58, 'batch65', 'Kavitha', '1', '9132440297', '2021-07-25 19:27:16', '2021-07-25 20:14:28', 1, '2021-07-25 19:27:16'),
(59, 'batch65', 'Lynette', '1', '6479271136', '2021-07-25 19:28:14', '2021-07-25 20:12:46', 1, '2021-07-25 19:28:14'),
(60, 'batch65', 'Atul', '1', '7326924593', '2021-07-25 19:35:14', '2021-07-25 19:41:16', 1, '2021-07-25 19:35:14'),
(61, 'batch65', 'Sanjay pachsiai', '973', '35010283', '2021-07-25 19:56:23', '2021-07-25 20:10:55', 1, '2021-07-25 19:39:50'),
(62, 'batch65', 'Umesh Kumar Vohra', '90', '5300697465', '2021-07-25 19:51:00', '2021-07-25 20:10:01', 1, '2021-07-25 19:51:00'),
(63, 'batch65', 'Sudha subramaniam ', '1', '5083662420', '2021-07-25 19:52:11', '2021-07-25 20:45:47', 1, '2021-07-25 19:52:11'),
(64, 'batch65', 'Indranil Pal', '48', '509776751', '2021-07-25 19:52:39', '2021-07-25 20:10:10', 1, '2021-07-25 19:52:39'),
(65, 'batch65', 'Satishchandra', '91', '9880414559', '2021-07-25 21:52:52', '2021-07-25 21:53:22', 1, '2021-07-25 20:28:01'),
(66, 'batch65', 'Anup', '44', '7760137674', '2021-07-25 20:41:32', '2021-07-25 20:41:55', 0, '2021-07-25 20:39:31'),
(67, 'batch65', 'andy', '', '9822295524', '2021-07-25 20:42:19', '2021-07-25 20:42:54', 0, '2021-07-25 20:42:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
