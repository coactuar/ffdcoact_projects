-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:29 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ExcerciseExpertQA_9`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `batch` varchar(50) NOT NULL,
  `select_batch` varchar(150) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `batch`, `select_batch`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `remark`) VALUES
(1, 'batch65', 'Batch70', 'vibhav', '9807164235', 'Hii', '2021-08-09 16:42:10', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(2, 'batch65', 'Batch70', 'vibhav', '9807164235', 'Hii', '2021-08-09 16:42:19', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(3, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'what is carb cycle?', '2021-08-22 19:01:40', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(4, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'I am in batch 70 IRP...I was doing well for a while and then suddenly slipped from the weight loss as I could not do SNS very well and then I have put on 2 kgs after I lost 9.5 kgs. How do I reverse this', '2021-08-22 19:02:43', '', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(5, 'batch65', 'Batch70', 'anupama', '9705666633', 'Hi doc. Quick question with regards to diet. I am from batch 70.  Should the diet depend on what exercise has been done for the day.\r\nI am yet to get my diet plan..it has been a week...Geeta says it needs to be approved by you and hence the delay', '2021-08-22 19:02:46', '', 0, 0, 0, 0, 0, 1, 0, 0, '3:2:1'),
(6, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Good evening am from B-70 in our consolidated blood reports why dont we carry out the Insulin Resistance test apart from the H1bAC?\r\nBesides what levels of fats and what % of fat do we lose across the program especially visceral fat and also how do we keep that visceral fat away?\r\nFYI - i have yet to hit the 5 BMI and am on the plan and am non-diabetic and am continuing with the old exercise regime, should i  hit the road with 3-2-1?', '2021-08-22 19:05:45', '65', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(7, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Can you explain 3 2 1 workout example with cardio( treadmill/stationery cycle/ elliptical )gym( with a trainer) and yoga', '2021-08-22 19:06:23', '', 0, 0, 1, 0, 0, 1, 0, 0, NULL),
(8, 'batch65', 'Batch70', 'anupama', '9705666633', 'Any restrictions on the veggies and fruits?', '2021-08-22 19:07:07', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(9, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'Batch 70: 3:2:1 totals to 6 days of recommended set of exercises, what about the 7th day?', '2021-08-22 19:09:10', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(10, 'batch65', 'Batch70', 'anupama', '9705666633', 'Do we continue with smoothie, soup...sorry all my questions revolve around food', '2021-08-22 19:10:01', '', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(11, 'batch65', 'Batch70', 'anupama', '9705666633', 'To lose weight, which type of exercise should be 3?', '2021-08-22 19:11:41', '', 0, 0, 0, 0, 1, 0, 0, 0, '3: Strength, 2: Cardio/Yoga'),
(12, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'So, just to clarify Dr, to lose weight I should do more strengthening 3 days and 2 days yoga and 1 day cardio..would that help?', '2021-08-22 19:12:03', '', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(13, 'batch65', 'Batch65', 'SWAMI GANGADHARAN', '9880239610', 'Sorry if this is a repeat question?  Is Carb Cycling for people who are trying to lose weight?', '2021-08-22 19:12:32', '', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(14, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', '3:2:1 - Covid is raging here in US so it is very risky to go to Gym. Is strength exercise possible at home with weights?', '2021-08-22 19:13:38', '1', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(15, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Doc re strength training will a functional trg of doing Core-Cardio-Abs and use body weights and TRX - pls advise', '2021-08-22 19:13:50', '65', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(16, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'I am non diabetic. How do I know I have achieved reversal?', '2021-08-22 19:14:17', '', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(17, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Thank you very much Dr Malhar. Please pass on my gratitude to the entire team', '2021-08-22 19:15:49', '', 0, 0, 0, 0, 1, 1, 0, 0, NULL),
(18, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'Batch 70: when I run 5K non stop, I feel very hungry.', '2021-08-22 19:16:00', '1', 0, 0, 0, 0, 1, 0, 0, 0, NULL),
(19, 'batch65', 'Batch70', 'anupama', '9705666633', 'We have recommended so many your way!! Thank you. It has been a life altering experience!!', '2021-08-22 19:17:09', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(20, 'batch65', 'Batch65', 'SWAMI GANGADHARAN', '9880239610', 'Doc Malhar, I know you dont recommend walking much.  But currently I have tendonitis in the left leg (behind the ankle).  So, physio has ruled out running for now?  Can we do walking for cardio?  If yes, what is the target we should aim for (during the 2 days)?', '2021-08-22 19:17:35', '', 0, 0, 0, 0, 0, 1, 0, 0, NULL),
(21, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Unable to see any quiz', '2021-08-22 19:18:07', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(22, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'Fried Chicken and Bowl of Sprouts', '2021-08-22 19:18:13', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(23, 'batch65', 'Batch70', 'anupama', '9705666633', 'unable to see', '2021-08-22 19:18:41', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(24, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'I am still seeing the questions tab', '2021-08-22 19:18:49', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(25, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Sorry the screen is yet showing the Q&A', '2021-08-22 19:19:08', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(26, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'My Lipid profile improved significantly at month 1 LDL 90. (I tested based on FFD recommendation). he 3rd month repeat lipid profile is similar to before starting LDL 120. Would love to sustain my first month results. How do I achieve that since I have a family history of major cardiac events', '2021-08-22 19:19:28', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(27, 'batch65', 'Batch70', 'anupama', '9705666633', 'Taco and coffee', '2021-08-22 19:19:50', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(28, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Mexican taco', '2021-08-22 19:19:53', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(29, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Mexican Tacos', '2021-08-22 19:19:54', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(30, 'batch65', 'Batch68', 'Caroline Cardoz', '9922297467', 'Tacos', '2021-08-22 19:20:47', '91', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(31, 'batch65', 'Batch69', 'Ishita Jhalani', '9414046691', 'Hi', '2021-08-22 19:20:57', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(32, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Sandwich would be may be 2nd choice', '2021-08-22 19:21:06', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(33, 'batch65', 'Batch65', 'SWAMI GANGADHARAN', '9880239610', 'Can we do Tai Chi instead of Yoga (or alternate between Yoga and Tai Chi)?', '2021-08-22 19:22:01', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(34, 'batch65', 'Batch70', 'anupama', '9705666633', 'Can we have eggs once in a while?', '2021-08-22 19:22:02', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(35, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'Sorry, my question did not get through fully. My LDL initially improved to 90, now back to 120. Cardiologist recommended level for me is below 100 because of family history. How do I achieve that', '2021-08-22 19:22:19', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(36, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Although vegan why has tofu been moved out of the overall diet ??', '2021-08-22 19:22:31', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(37, 'batch65', 'Batch70', 'anupama', '9705666633', 'Would you advice vegan diet as a life style?', '2021-08-22 19:23:58', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(38, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Why do we suffer from this huge hunger pang at around 5 PM', '2021-08-22 19:24:19', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(39, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'resistance band exercises: jsut want to check  - is that considered part of strength or stamina?', '2021-08-22 19:25:34', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(40, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Is there a phase 5 and if yes what would it be about...', '2021-08-22 19:26:25', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(41, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Circadian rhythm how to determine it that will be great if we can determine it', '2021-08-22 19:29:41', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(42, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'look forward for this ppt', '2021-08-22 19:30:29', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(43, 'batch65', 'Batch70', 'Sarika Nigam', '9787616519', 'What shuold be the duration of 3 2 1 exercises? 30 mins/45 mins?', '2021-08-22 19:30:52', '1', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(44, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'pea or soya or rice based protein powder', '2021-08-22 19:31:18', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(45, 'batch65', 'Batch69', 'Sanjaya', '9900098324', 'can I alternate', '2021-08-22 19:31:38', '', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(46, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'How do we improve the gut microbiome?', '2021-08-22 19:31:48', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(47, 'batch65', 'Batch70', 'anupama', '9705666633', 'Requesting the diet plan to be shared please. Thank you for everything. No more questions. Happy Rakshabandhan! Good night.', '2021-08-22 19:32:48', '', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(48, 'batch65', 'Batch70', 'Suchitra Raghunathan ', '9642332266', 'Yes', '2021-08-22 19:33:35', '', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(49, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Enjoy your festival today Doc YES', '2021-08-22 19:33:53', '65', 0, 0, 1, 0, 0, 0, 0, 0, NULL),
(50, 'batch65', 'Batch70', 'Amish Shah', '97687890', 'Hope to connect soon', '2021-08-22 19:34:04', '65', 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(51, 'batch65', 'Batch68', 'Caroline Cardoz', '9922297467', 'Thanks', '2021-08-22 19:34:33', '91', 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `batch` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `batch`, `name`, `cntry_code`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `joining_date`) VALUES
(1, 'batch65', 'vibhav', '', '9807164235', '2021-08-09 16:41:36', '2021-08-09 16:42:51', 0, '2021-08-09 16:41:36'),
(2, 'batch65', 'Savio Sequeira', '91', '09920781418', '2021-08-17 18:14:54', '2021-08-17 18:15:55', 1, '2021-08-17 18:14:54'),
(3, 'batch65', 'Zeenal vora', '', '8263814312', '2021-08-18 12:26:59', '2021-08-18 12:27:29', 1, '2021-08-18 12:26:59'),
(4, 'batch65', 'Nishanth', '', '554561', '2021-08-22 17:34:48', '2021-08-22 18:24:35', 0, '2021-08-22 17:34:48'),
(5, 'batch65', 'Nishanth', '', '1', '2021-08-22 18:24:43', '2021-08-22 18:45:40', 0, '2021-08-22 18:24:43'),
(6, 'batch65', 'Sarika Nigam', '1', '9787616519', '2021-08-22 18:42:17', '2021-08-22 19:34:48', 1, '2021-08-22 18:42:17'),
(7, 'batch65', 'Nishanth', '', '55', '2021-08-22 18:45:53', '2021-08-22 18:57:44', 0, '2021-08-22 18:45:53'),
(8, 'batch65', 'Nishanth', '', '565', '2021-08-22 18:57:55', '2021-08-22 19:02:02', 1, '2021-08-22 18:57:55'),
(9, 'batch65', 'Suchitra Raghunathan ', '', '9642332266', '2021-08-22 18:58:58', '2021-08-22 19:34:58', 1, '2021-08-22 18:58:58'),
(10, 'batch65', 'Lakshmi Gannavarapu', '91', '9866693044', '2021-08-22 19:00:21', '2021-08-22 19:34:52', 1, '2021-08-22 19:00:21'),
(11, 'batch65', 'anupama', '', '9705666633', '2021-08-22 19:00:35', '2021-08-22 19:34:36', 1, '2021-08-22 19:00:35'),
(12, 'batch65', 'Amish Shah', '65', '97687890', '2021-08-22 19:01:08', '2021-08-22 19:35:08', 1, '2021-08-22 19:01:08'),
(13, 'batch65', 'Nishanth', '', '22', '2021-08-22 19:01:44', '2021-08-22 19:01:53', 0, '2021-08-22 19:01:44'),
(14, 'batch65', 'SWAMI GANGADHARAN', '', '9880239610', '2021-08-22 19:05:19', '2021-08-22 19:34:49', 1, '2021-08-22 19:05:19'),
(15, 'batch65', 'Sanjaya', '', '9900098324', '2021-08-22 19:13:11', '2021-08-22 19:38:40', 1, '2021-08-22 19:05:32'),
(16, 'batch65', 'Caroline Cardoz', '91', '9922297467', '2021-08-22 19:08:34', '2021-08-22 20:12:12', 1, '2021-08-22 19:08:34'),
(17, 'batch65', 'Purvi shah', '91', '9819133846', '2021-08-22 19:09:19', '2021-08-22 19:35:20', 1, '2021-08-22 19:09:19'),
(18, 'batch65', 'Nitin', '', '9440685113', '2021-08-22 19:10:08', '2021-08-22 19:52:02', 1, '2021-08-22 19:10:08'),
(19, 'batch65', 'Sheetal BS', '91', '9449153968', '2021-08-22 19:14:54', '2021-08-22 19:40:24', 1, '2021-08-22 19:14:54'),
(20, 'batch65', 'Ishita Jhalani', '', '09414046691', '2021-08-22 19:16:50', '2021-08-22 19:21:17', 1, '2021-08-22 19:16:50'),
(21, 'batch65', 'Ishita Jhalani', '', '9414046691', '2021-08-22 19:20:48', '2021-08-22 19:22:17', 1, '2021-08-22 19:20:48'),
(22, 'batch65', 'Bindu Khatri', '91', '9971198916', '2021-08-22 19:22:04', '2021-08-22 19:35:04', 1, '2021-08-22 19:22:04'),
(23, 'batch65', 'Varuni', '', '9811173445', '2021-08-22 20:37:06', '2021-08-22 20:46:58', 1, '2021-08-22 20:37:06'),
(24, 'batch65', 'Sonal', '44', '7971918740', '2021-08-25 14:51:11', '2021-08-25 17:33:34', 1, '2021-08-25 14:51:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
