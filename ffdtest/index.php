<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FFD 25 April :: Testing Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body>
<div class="container-fluid">
    <div class="row video-panel">
        <div class="col-12 col-md-8 offset-md-2 mt-3">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no" autoplay fullscreen></iframe>
          </div>
        </div>
        
    </div>
    
</div>

<div class="row">

</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>




</body>
</html>