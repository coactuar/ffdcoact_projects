<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
    background-color:#fff;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
myPlayer = WowzaPlayer.create('videoPlayer',
    {
    "license":"PLAY2-f9DvN-9hUXF-64dZB-j3Vaw-ERHJp",
   /* "sources":[{
    "sourceURL":"https://5e100d36d6257.streamlock.net/vod/_definst_/smil:ffd_29jan.smil/playlist.m3u8"
    },
    {
    "sourceURL":"https://5e100d36d6257.streamlock.net/vod/mp4:ffd_29jan.mp4/playlist.m3u8"
    }],*/
    "autoPlay":true,
    "mute":false,
    "volume":75
    }
);
//myPlayer.play();
</script>
</body>
</html>