<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
myPlayer = WowzaPlayer.create('videoPlayer',
    {
    "license":"PLAY1-49Hm6-HkQDk-V9h4d-9cHaw-QyhF7",
    "sourceURL":"https://5e100d36d6257.streamlock.net/vod/mp4:ffd_17jan.mp4/playlist.m3u8",
    "autoPlay":false,
    "volume":"75",
    "mute":false,
    "loop":false,
    "audioOnly":false,
    "stringErrorStreamUnavailable" : "Please try again later.",
    }
);
//myPlayer.play();
</script>
</body>
</html>