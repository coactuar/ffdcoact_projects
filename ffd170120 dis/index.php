<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
</nav>
<div class="container">
    <div class="row mt-2">
        <div class="col-12 text-center">
            <div class="bg-black">
                <h3 class="title">ONLINE FIRST SESSION of Intensive Reversal Program</h3>
                <h4 class="dr-text">by Dr. Pramod Tripathi</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 text-center">
            <div class="event-info">
                <img src="img/pramod.jpg" class="img-fluid dr-photo"  alt=""/>
                <h4 class="purple-text">Date:</h4>
                <h4>17th Jan 2020</h4>
                <h4 class="purple-text">Time:</h4>
                <h4>7pm to 9pm (IST)</h4>
            </div>
        </div>
        <div class="col-12 col-md-6 offset-md-1 text-center">
            <form id="login-form" method="post">
            <h1>Login</h1>
              <div id="login-message"></div>
              <div class="input-group mt-1 mb-1">
                <input type="text" class="form-control" placeholder="Your Name" aria-label="Your Name" aria-describedby="basic-addon1" name="name" id="name" required>
              </div>
              <div class="input-group mt-1 mb-1">
                <input type="text" class="form-control" placeholder="Your Phone Number" aria-label="Your Phone Number" aria-describedby="basic-addon1" name="phnNum" id="phnNum" required>
              </div>
              <div class="input-group mt-1 mb-1">
                <button class="mt-4 btn btn-block" type="submit">Login</button>
              </div>
            </form>
        </div>
    </div>
    <div class="row mt-3 mb-3">
        <div class="col-12 text-center">
            <div class="icons bg-black">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#country').val() == '-1')
    {
        alert('Please select country code');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your phone numer is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'webcast.php';   
      }
      
  });
  
  return false;
});

});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>