<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
</nav>
<div class="container">
    <div class="row mt-2 mb-2">
        <div class="col-12 text-center">
            <h3 class="title">Online First Session of Intensive Reversal Program</h3>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-md-6 text-center">
                <img src="img/pramod.jpg" class="img-fluid dr-photo"  alt=""/> 
        </div>

        <div class="col-12 col-md-6">
            <form id="login-form" method="post">
            <h1>Login</h1>
              <div id="login-message"></div>
              <div class="input-group">
                <select id="country" name="country" required>
                    <option value="-1">Select Country Code</option>
                    <?php
                    $query="SELECT DISTINCT cntry_code FROM tbl_users order by cntry_code asc";
                    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
                    while($data = mysqli_fetch_assoc($res))
                    {
                     ?>
                     <option value="<?php echo $data['cntry_code']; ?>"><?php echo $data['cntry_code']; ?></option>
                     <?php   
                    }
                    ?>
                </select>
                <input type="text" class="form-control" placeholder="Your Phone Number" aria-label="Your Phone Number" aria-describedby="basic-addon1" name="phnNum" id="phnNum" required>
              </div>
              
              <button class="mt-4" type="submit">Login</button>
            </form>
            <h4>2 hours Video Webinar</h4>
            <h5>by Dr. Pramod Tripathi</h5>
            <h6>
            <u>Date:</u> 4th January 2020<br>
            <u>Time:</u> 9am to 11am (IST)<br>
            </h6>
        </div>
    </div>
    <div class="row mt-3 mb-3">
        <div class="col-12 text-center">
            <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#country').val() == '-1')
    {
        alert('Please select country code');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your phone numer is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'webcast.php';   
      }
      
  });
  
  return false;
});

});

</script>
</body>
</html>