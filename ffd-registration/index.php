<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Registration</title>
    <link rel="stylesheet" href="styles.css">

</head>

<body>
    <div class="container bg-image">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <img src="img/welcomecr.png" class="imgwel1">

            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form id="form1">
                    <div class="form-row mg">
                        <legend class="m-4">Please fill in the Registration Details</legend>
                        <div class="form-group col-md-6">
                            <label for="inputFirstName">FIRST NAME</label>
                            <input type="text" class="form-control fname" id="inputfname">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLastName">LAST NAME</label>
                            <input type="text" class="form-control lname" id="inputlname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="mg ml-2">EMAIL</label>
                        <input type="email" class="form-control email" id="inputEmail">
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6 ">
                            <label for="inputEmail4">MOBILE NO.</label>
                            <input type="number" class="form-control mobile" id="inputMobile">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCity">CITY</label>
                            <select id="inputCity" class="form-control">
                            <option selected>Select City</option>
                            <option>...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6">
                            <label for="inputState">STATE</label>
                            <select id="inputState" class="form-control">
                            <option selected>Select State</option>
                            <option>...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCountry">COUNTRY</label>
                            <select id="inputCountry" class="form-control">
                            <option selected>Select Country</option>
                            <option>...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group mg">
                        <label for="inputProgram" class="lebel">HAVE YOU ATTENDED ANY OF THE FFD PROGRAM</label>
                        <select id="inputProgram" class="form-control program">
                        <option selected>NOT ATTENDED ANY OF PROGRAM</option>
                        <option>FIRST SESSION/FOUNDATION/ONLINE FIRST CLASS</option>
                        <option>INTENSIVE REVERSAL PROGRAM (IRP)</option>
                        <option>TRANCENDENTAL RESIDENSTIAL PROGRAM (TRP)</option>
                        </select>
                    </div>
                    <div class="form-group mg">
                        <label for="inputAbout" class="lebel">FROM WHERE DID YOU COME TO KNOW ABOUT THE EVENT?</label>
                        <select id="inputAbout" class="form-control about">
                        <option>...</option>
                        <option>...</option>
                        </select>
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6 col-sm-6">
                            <button type="submit" class="btn btn1 mg">REGISTRATION</button>
                            <button type="submit" class="btn btn2 mg">CANCEL</button>
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="form-group col-md-4 col-sm-4">
                            <img src="img/birdffdcr.png" class="logo" alt="logo">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>

    </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>