-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 07, 2022 at 08:27 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ffd_080122`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(50) NOT NULL,
  `responded` int(11) DEFAULT '0',
  `doctor` int(11) DEFAULT '0',
  `diet` int(11) DEFAULT '0',
  `exercise` int(11) DEFAULT '0',
  `mentor` int(11) DEFAULT '0',
  `operations` int(11) DEFAULT '0',
  `InnerTransformation` int(11) DEFAULT '0',
  `Sales` int(11) DEFAULT '0',
  `Program` int(11) DEFAULT '0',
  `remark` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `eventname`, `responded`, `doctor`, `diet`, `exercise`, `mentor`, `operations`, `InnerTransformation`, `Sales`, `Program`, `remark`) VALUES
(1, 'Narender kumar ', '8733817870', 'I am taking insulin since Jan,2014 will it helps', '2021-12-30 13:57:53', '', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(2, 'shriram', '7774021933', 'no', '2022-01-01 09:13:53', '', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(3, 'P.REDDI PRASAD NAIDU', '9963688478', 'I AM GETTING FOOT FINGER AREA PAIN REASONS AND HOW TO AVOID IT, IS IT A DIABETIC SEVERE LEVEL SYMTOM', '2022-01-02 11:16:07', '91', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(4, 'P.REDDI PRASAD NAIDU', '9963688478', 'IN WHAT  WAY I CAN STRICTLY CONTROL BY DIABETC', '2022-01-02 11:17:44', '91', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(5, 'Ranjini Sridhar', '9008032163', 'How to manage diabetes with sustainable diet.  Ketogenic diet is difficult to continue.  Especially with keto atta', '2022-01-05 20:54:10', '', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(6, 'Dhananjay Shivgonda Khot', '09890948284', 'Is there any permanent cure solution for diabetes?', '2022-01-06 20:20:08', '', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(7, 'Satish Singh chib', '7006330783', 'Iam 39years old iam debatics from last 10years how I reverse  my diabetes I have problem in legs sansiation is low iam gym going persons I take medicine  galvasmet 850mg in morning and gemer F1 in evening sugar almost in control', '2022-01-07 08:49:49', '91', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(8, 'Madhu verma ', '9891448111', 'My fasting suger  remaining 150 to18o. And Hb1c 8.7.Acidity always high. Pain in knees and small joints. Dib.isfrom last 8years and thyroid from last 20 years.  My age is 57. Hight 5.2. Weight 72.', '2022-01-07 09:36:12', '', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL),
(9, 'Shalini meshram', '9860032131', 'I hv hypothyroidism and My Hba1c value is 7.30 but not started medicine yet. Am I considered as diabetic or pre diabetic ', '2022-01-07 09:48:31', '91', 0, 0, 'ffd050121', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reg_date` datetime NOT NULL,
  `mobile_num` varchar(20) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `cntry_code` varchar(10) DEFAULT NULL,
  `eventname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `reg_date`, `mobile_num`, `login_date`, `logout_date`, `logout_status`, `cntry_code`, `eventname`) VALUES
(1, 'Vibhav ', '2021-12-29 14:06:45', '9764249239', '2021-12-29 14:06:45', '2021-12-29 14:08:49', 0, '', 'ffd050121'),
(2, 'Satish Singh chib', '2021-12-29 16:07:15', '7006330783', '2022-01-07 08:43:29', '2022-01-07 08:54:21', 1, '91', 'ffd050121'),
(3, 'Keerthi Varudu', '2021-12-30 12:30:58', '8985855062', '2021-12-30 12:30:58', '2021-12-30 12:38:03', 1, '', 'ffd050121'),
(4, 'Narender kumar ', '2021-12-30 13:56:29', '8733817870', '2021-12-30 13:56:29', '2021-12-30 13:58:31', 1, '', 'ffd050121'),
(5, 'Ashok rastogi', '2021-12-31 20:23:38', '7077756152', '2022-01-01 10:38:43', '2022-01-01 10:43:44', 1, '91', 'ffd050121'),
(6, 'Jayashree subbarayan', '2022-01-01 07:02:43', '9629973822', '2022-01-01 07:02:43', '2022-01-01 07:03:44', 1, '', 'ffd050121'),
(7, 'Shriram joshi', '2022-01-01 08:30:20', '7774021933', '2022-01-01 09:11:54', '2022-01-01 09:15:12', 1, '', 'ffd050121'),
(8, 'Amogh Deshpande', '2022-01-01 09:31:25', '9403645816', '2022-01-01 09:33:42', '2022-01-01 09:33:59', 0, '', 'ffd050121'),
(9, 'R LAKSHMIPATHI', '2022-01-01 19:03:28', '09381066691', '2022-01-01 19:03:28', '2022-01-01 19:35:51', 1, '91', 'ffd050121'),
(10, 'Syanta Deb', '2022-01-01 23:09:09', '07005168810', '2022-01-01 23:09:09', '2022-01-01 23:09:39', 1, '', 'ffd050121'),
(11, 'Jeevana B. Nayak', '2022-01-02 09:14:48', '914833702', '2022-01-02 09:14:48', '2022-01-02 09:16:08', 1, '91', 'ffd050121'),
(12, 'P.REDDI PRASAD NAIDU', '2022-01-02 11:12:25', '9963688478', '2022-01-02 11:12:25', '2022-01-02 11:17:55', 1, '91', 'ffd050121'),
(13, 'Avelino Vaz', '2022-01-02 12:33:01', '09168662802', '2022-01-02 12:33:01', '2022-01-02 12:35:13', 1, '', 'ffd050121'),
(14, 'Ramesh', '2022-01-02 17:02:08', '7010809716', '2022-01-02 17:02:08', '2022-01-02 17:03:09', 1, '', 'ffd050121'),
(15, 'Anandaraman', '2022-01-02 21:29:02', '7204571072', '2022-01-02 21:29:02', '2022-01-02 21:29:32', 1, '91', 'ffd050121'),
(16, 'Usha Ganesh', '2022-01-02 22:36:09', '9397681967', '2022-01-05 19:08:56', '2022-01-05 19:14:33', 1, '', 'ffd050121'),
(17, 'BHAVANA VENKATA RAMALINGESWARA RAO', '2022-01-03 05:23:12', '9866808977', '2022-01-03 05:23:12', '2022-01-03 05:23:47', 0, '', 'ffd050121'),
(18, 'SUBHADIPTA LENKA', '2022-01-03 07:38:11', '7327838785', '2022-01-03 07:38:11', '2022-01-03 07:39:12', 1, '', 'ffd050121'),
(19, 'Shiv Shankar Prasad', '2022-01-03 08:21:09', '9449157177', '2022-01-03 08:21:09', '2022-01-03 08:23:09', 1, '', 'ffd050121'),
(20, 'Chinthalaphani Murlidhar Reddy', '2022-01-03 08:49:05', '9989083999', '2022-01-03 08:49:05', '2022-01-03 08:52:06', 1, '91', 'ffd050121'),
(21, 'Raman Kullakanda', '2022-01-03 11:05:57', '9030541044', '2022-01-03 11:05:57', '2022-01-03 11:07:29', 1, '', 'ffd050121'),
(22, 'ASISH KUMAR SAMANTARAY', '2022-01-03 11:31:23', '9861026291', '2022-01-03 11:31:23', '2022-01-03 11:31:50', 0, '', 'ffd050121'),
(23, 'Anoopa', '2022-01-03 12:09:04', '8247234513', '2022-01-03 12:09:04', '2022-01-03 12:11:03', 0, '91', 'ffd050121'),
(24, 'Dr.Shahaji Sawant', '2022-01-03 21:27:50', '9766128469', '2022-01-03 21:27:50', '2022-01-03 21:30:51', 1, '', 'ffd050121'),
(25, 'Srinivas Ashok Bheemisetty', '2022-01-03 21:31:36', '7408031916', '2022-01-03 21:31:36', '2022-01-03 21:31:54', 0, '1', 'ffd050121'),
(26, 'Mayur', '2022-01-03 21:32:25', '9819271427', '2022-01-03 21:32:25', '2022-01-03 21:32:55', 1, '', 'ffd050121'),
(27, 'Sharada Nerella ', '2022-01-03 21:45:20', '09866047777', '2022-01-03 21:45:20', '2022-01-03 21:47:51', 1, '', 'ffd050121'),
(28, 'Jeevana B. Nayak', '2022-01-04 09:02:40', '9148337020', '2022-01-04 09:02:40', '2022-01-04 09:05:57', 1, '91', 'ffd050121'),
(29, 'vineet', '2022-01-04 09:17:56', '9026277553', '2022-01-04 09:17:56', '2022-01-04 09:18:57', 1, '91', 'ffd050121'),
(30, 'Sridha CK', '2022-01-04 10:00:06', '9538998628', '2022-01-04 10:01:33', '2022-01-04 10:02:54', 0, '', 'ffd050121'),
(31, 'Mahefooz  Malladi', '2022-01-04 11:33:03', '9923536990', '2022-01-04 11:33:03', '2022-01-04 11:33:38', 0, '', 'ffd050121'),
(32, 'Manoj Dadlani', '2022-01-04 12:22:00', '9827055042', '2022-01-04 12:22:00', '2022-01-04 12:26:02', 1, '', 'ffd050121'),
(33, 'Ramesh', '2022-01-04 12:41:53', '9448389450', '2022-01-04 12:41:53', '2022-01-04 12:42:23', 1, '91', 'ffd050121'),
(34, 'G V RAJASEKHARA RAO', '2022-01-04 12:51:26', '9886787799', '2022-01-04 12:51:26', '2022-01-04 12:51:56', 1, '91', 'ffd050121'),
(35, 'showkat shafi Sheikh', '2022-01-04 12:52:52', '08899546373', '2022-01-04 12:52:52', '2022-01-04 12:53:22', 1, '91', 'ffd050121'),
(36, 'Sankaranarayanan. T. A. ', '2022-01-04 13:32:31', '9962025852', '2022-01-04 13:32:31', '2022-01-04 13:34:01', 1, '91', 'ffd050121'),
(37, 'Neelmani Ghimire', '2022-01-04 13:40:12', '8390955110', '2022-01-04 13:40:12', '2022-01-04 13:40:42', 1, '', 'ffd050121'),
(38, 'Vibha Singh', '2022-01-04 13:43:05', '9927081719', '2022-01-04 13:43:05', '2022-01-04 13:43:35', 1, '', 'ffd050121'),
(39, 'Deepthi S', '2022-01-04 13:50:50', '09739229977', '2022-01-04 13:50:50', '2022-01-04 13:51:20', 1, '', 'ffd050121'),
(40, 'JEGADEESWARAN K', '2022-01-04 13:56:31', '9486873124', '2022-01-06 14:18:41', '2022-01-06 14:19:11', 1, '', 'ffd050121'),
(41, 'Guru Nathan', '2022-01-04 14:00:53', '09629090045', '2022-01-04 14:00:53', '2022-01-04 14:11:43', 1, '91', 'ffd050121'),
(42, 'Dr. Milan Modi', '2022-01-04 14:30:58', '9727715152', '2022-01-04 14:30:58', '2022-01-04 14:33:44', 1, '', 'ffd050121'),
(43, 'G SUBRAMANIAN', '2022-01-04 15:23:03', '9868236348', '2022-01-04 15:23:03', '2022-01-04 15:26:34', 1, '', 'ffd050121'),
(44, 'OMPRAKASH GUDAPARTHI', '2022-01-04 15:33:21', '9112220118', '2022-01-04 15:33:21', '2022-01-04 15:34:22', 1, '', 'ffd050121'),
(45, 'Akshay Singhi', '2022-01-04 17:10:32', '9051260126', '2022-01-04 17:10:32', '2022-01-04 17:10:47', 0, '', 'ffd050121'),
(46, 'Hema Shreekuamr', '2022-01-04 17:13:22', '9901115090', '2022-01-04 19:15:59', '2022-01-04 19:17:00', 1, '', 'ffd050121'),
(47, 'Venkatramanrao Gadde', '2022-01-04 18:13:24', '9960814502', '2022-01-04 18:13:24', '2022-01-04 18:14:39', 0, '91', 'ffd050121'),
(48, 'Mahendra Chirawawala', '2022-01-04 18:38:55', '09821874562', '2022-01-04 18:38:55', '2022-01-04 18:39:25', 1, '91', 'ffd050121'),
(49, 'PRAMOD CHAUDHARY', '2022-01-04 18:50:16', '9918828487', '2022-01-04 18:50:16', '2022-01-04 18:50:46', 1, '', 'ffd050121'),
(50, 'Ritu', '2022-01-04 18:58:15', '9906131055', '2022-01-04 18:58:15', '2022-01-04 19:34:36', 1, '', 'ffd050121'),
(51, 'Hansa Agrawal', '2022-01-04 19:06:45', '9890678022', '2022-01-04 19:06:45', '2022-01-04 19:07:15', 1, '', 'ffd050121'),
(52, 'PREMILA DINESHKUMAR ', '2022-01-04 19:10:18', '9980804748', '2022-01-04 19:10:18', '2022-01-04 19:21:50', 1, '', 'ffd050121'),
(53, 'Satheesh D ', '2022-01-04 19:27:56', '9778556791', '2022-01-04 19:29:02', '2022-01-04 19:30:20', 1, '91', 'ffd050121'),
(54, 'NEERAJ KUMAR ', '2022-01-04 20:56:22', '8987618228', '2022-01-04 20:56:22', '2022-01-04 20:57:23', 1, '91', 'ffd050121'),
(55, 'Mr. Yogachandar P A', '2022-01-04 22:30:45', '09677104617', '2022-01-06 12:29:55', '2022-01-06 12:39:19', 1, '', 'ffd050121'),
(56, 'Kiran kumar ', '2022-01-04 22:52:47', '9773969360', '2022-01-04 22:52:47', '2022-01-04 22:53:48', 1, '965', 'ffd050121'),
(57, 'Khushboo Kewlani', '2022-01-04 23:54:25', '8889107330', '2022-01-04 23:54:25', '2022-01-04 23:59:57', 1, '', 'ffd050121'),
(58, 'Pranav Garg', '2022-01-05 00:12:12', '9999602225', '2022-01-05 00:12:12', '2022-01-05 00:17:33', 1, '', 'ffd050121'),
(59, 'Rajat Behl', '2022-01-05 00:29:52', '9905132408', '2022-01-05 00:29:52', '2022-01-05 00:36:24', 1, '91', 'ffd050121'),
(60, 'Hajira Fatima', '2022-01-05 01:02:36', '08501840720', '2022-01-05 01:02:36', '2022-01-05 01:03:06', 1, '', 'ffd050121'),
(61, 'Shreekant bhikaji chavan', '2022-01-05 01:13:24', '09096370668', '2022-01-05 01:13:24', '2022-01-05 01:13:54', 1, '91', 'ffd050121'),
(62, 'Rajiv Agarwal', '2022-01-05 05:52:47', '9800079444', '2022-01-05 05:52:47', '2022-01-05 05:53:47', 1, '', 'ffd050121'),
(63, 'Dr.Prabhu P', '2022-01-05 05:56:02', '9789973005', '2022-01-05 05:56:02', '2022-01-05 06:02:30', 1, '91', 'ffd050121'),
(64, 'Chandra Prabha CR', '2022-01-05 06:55:23', '8008162018', '2022-01-05 06:55:23', '2022-01-05 06:55:46', 0, '', 'ffd050121'),
(65, 'Sri Harsha Pulleti', '2022-01-05 09:42:37', '8479028196', '2022-01-05 09:42:37', '2022-01-05 10:04:24', 1, '1', 'ffd050121'),
(66, 'Jeevana B. Nayak', '2022-01-05 09:47:48', '9148337020', '2022-01-05 09:47:48', '2022-01-05 09:48:18', 1, '', 'ffd050121'),
(67, 'Ashok Kumar N ', '2022-01-05 12:14:36', '8722299846', '2022-01-05 12:14:36', '2022-01-05 12:15:03', 0, '91', 'ffd050121'),
(68, 'M. Umamuni', '2022-01-05 12:31:37', '9865047216', '2022-01-05 12:31:37', '2022-01-05 12:32:07', 1, '', 'ffd050121'),
(69, 'Vanajamurugesh', '2022-01-05 12:52:15', '9964430851', '2022-01-05 12:52:15', '2022-01-05 12:52:45', 1, '', 'ffd050121'),
(70, 'p v srinivas', '2022-01-05 13:13:04', '9444828795', '2022-01-05 13:13:04', '2022-01-05 13:13:18', 0, '91', 'ffd050121'),
(71, 'Dandamudi Purnachandra Rao', '2022-01-05 14:21:47', '9966175733', '2022-01-05 14:21:47', '2022-01-05 14:27:52', 1, '', 'ffd050121'),
(72, 'Sachin Sahebrao Patil', '2022-01-05 15:11:48', '9881235688', '2022-01-05 15:11:48', '2022-01-05 15:12:52', 1, '', 'ffd050121'),
(73, 'S.Somasundaram', '2022-01-05 15:31:53', '9080357523', '2022-01-05 15:31:53', '2022-01-05 15:32:54', 1, '', 'ffd050121'),
(74, 'Vinaay W', '2022-01-05 16:07:07', '9867442004', '2022-01-05 16:07:07', '2022-01-05 20:27:11', 1, '91', 'ffd050121'),
(75, 'Mahesh M', '2022-01-05 16:24:38', '09900995554', '2022-01-05 16:24:38', '2022-01-05 16:25:08', 1, '', 'ffd050121'),
(76, 'Sanjaya Ranjana Mohapatra', '2022-01-05 16:45:15', '9702044832', '2022-01-05 16:45:15', '2022-01-05 17:28:22', 1, '', 'ffd050121'),
(77, 'Neela Deshpande', '2022-01-05 17:20:56', '9960562673', '2022-01-05 17:20:56', '2022-01-05 17:23:05', 1, '', 'ffd050121'),
(78, 'Matheen Ahmed ', '2022-01-05 17:28:19', '9845274100', '2022-01-05 17:28:19', '2022-01-05 17:32:21', 1, '', 'ffd050121'),
(79, 'Bharani Subhash ', '2022-01-05 18:14:10', '9966944556', '2022-01-05 18:14:10', '2022-01-05 18:14:33', 0, '', 'ffd050121'),
(80, 'Ranjini Sridhar', '2022-01-05 20:52:55', '9008032163', '2022-01-05 20:52:55', '2022-01-05 20:55:26', 1, '', 'ffd050121'),
(81, 'Mr. Sunil Kumar Manocha', '2022-01-05 21:52:42', '9867009713', '2022-01-05 21:52:42', '2022-01-05 21:53:10', 0, '', 'ffd050121'),
(82, 'MADHUSUDAN BEHERA ', '2022-01-05 22:02:54', '9437269430', '2022-01-05 22:02:54', '2022-01-05 22:03:24', 1, '91', 'ffd050121'),
(83, 'chandrasekharappa', '2022-01-06 02:47:13', '2406722115', '2022-01-06 02:47:13', '2022-01-06 02:48:14', 1, '1', 'ffd050121'),
(84, 'Rajesh kumar sahu', '2022-01-06 07:32:55', '9937867769', '2022-01-06 07:32:55', '2022-01-06 07:33:16', 0, '', 'ffd050121'),
(85, 'Veena Deshmukh', '2022-01-06 09:12:55', '7416707934', '2022-01-06 09:12:55', '2022-01-06 09:13:55', 1, '', 'ffd050121'),
(86, 'Rajendra Kamble', '2022-01-06 11:45:18', '9764656088', '2022-01-06 11:45:18', '2022-01-06 11:46:58', 1, '', 'ffd050121'),
(87, 'Dr Sundaresan ', '2022-01-06 13:38:20', '8754900758', '2022-01-06 13:38:20', '2022-01-06 14:24:36', 1, '', 'ffd050121'),
(88, 'Baskaran', '2022-01-06 14:52:21', '09486101815', '2022-01-06 14:52:21', '2022-01-06 14:53:17', 0, '', 'ffd050121'),
(89, 'GAURIKA', '2022-01-06 14:58:06', '8148688180', '2022-01-06 14:58:06', '2022-01-06 14:58:36', 1, '', 'ffd050121'),
(90, 'Rajesh Bhasin ', '2022-01-06 15:49:16', '9419185945', '2022-01-06 15:49:16', '2022-01-06 15:51:18', 1, '', 'ffd050121'),
(91, 'Krishna ', '2022-01-06 15:55:51', '433487242', '2022-01-06 15:55:51', '2022-01-06 16:27:13', 1, '61', 'ffd050121'),
(92, 'Ranjini Guruprasad', '2022-01-06 16:15:49', '9900134509', '2022-01-06 16:15:49', '2022-01-06 16:21:19', 1, '91', 'ffd050121'),
(93, 'Pari', '2022-01-06 16:22:41', '506780207', '2022-01-06 16:22:41', '2022-01-06 16:23:11', 1, '971', 'ffd050121'),
(94, 'Neeraj Patel', '2022-01-06 16:35:18', '9406718187', '2022-01-06 16:35:18', '2022-01-06 16:35:48', 1, '', 'ffd050121'),
(95, 'RAVINDRA U MEHTA', '2022-01-06 18:28:20', '9820135645', '2022-01-06 18:28:20', '2022-01-06 18:28:35', 0, '91', 'ffd050121'),
(96, 'Pagadala Sivapradeep', '2022-01-06 19:05:59', '09849664892', '2022-01-06 19:05:59', '2022-01-06 19:06:29', 1, '', 'ffd050121'),
(97, 'Dhananjay Shivgonda Khot', '2022-01-06 20:19:10', '09890948284', '2022-01-06 20:19:10', '2022-01-06 20:20:41', 1, '', 'ffd050121'),
(98, 'Priyanka Ranka', '2022-01-06 22:41:38', '09892013213', '2022-01-06 22:41:38', '2022-01-06 22:45:06', 1, '', 'ffd050121'),
(99, 'Chandan dulhani', '2022-01-07 00:19:11', '9457619988', '2022-01-07 00:19:11', '2022-01-07 00:21:12', 1, '', 'ffd050121'),
(100, 'Naik', '2022-01-07 01:20:43', '09945568155', '2022-01-07 01:20:43', '2022-01-07 01:21:45', 1, '', 'ffd050121'),
(101, 'Rakkappan', '2022-01-07 05:23:34', '6134838877', '2022-01-07 05:23:34', '2022-01-07 05:29:40', 1, '1', 'ffd050121'),
(102, 'Vajri Iyer', '2022-01-07 07:50:17', '4163037565', '2022-01-07 07:50:17', '2022-01-07 07:54:50', 1, '1', 'ffd050121'),
(103, 'V K KASHYAP', '2022-01-07 07:59:30', '09654566066', '2022-01-07 08:07:39', '2022-01-07 08:09:52', 0, '91', 'ffd050121'),
(104, 'P vijay Anil kumar', '2022-01-07 08:08:26', '07416659921', '2022-01-07 08:08:26', '2022-01-07 08:09:58', 1, '91', 'ffd050121'),
(105, 'V K KASHYAP', '2022-01-07 08:09:32', '09654566066', '2022-01-07 08:10:12', '2022-01-07 08:11:15', 1, '', 'ffd050121'),
(106, 'Balakrishna D', '2022-01-07 09:06:32', '9449130679', '2022-01-07 09:06:32', '2022-01-07 09:07:33', 1, '91', 'ffd050121'),
(107, 'Madhu verma ', '2022-01-07 09:25:44', '9891448111', '2022-01-07 09:25:44', '2022-01-07 09:41:10', 1, '', 'ffd050121'),
(108, 'Chandan ', '2022-01-07 09:36:45', '8792825923', '2022-01-07 09:36:45', '2022-01-07 09:39:03', 1, '', 'ffd050121'),
(109, 'Shalini meshram', '2022-01-07 09:47:01', '9860032131', '2022-01-07 09:47:01', '2022-01-07 09:49:02', 1, '91', 'ffd050121'),
(110, 'Sanjay Rai', '2022-01-07 09:57:05', '9564653377', '2022-01-07 09:57:05', '2022-01-07 09:58:20', 0, '', 'ffd050121'),
(111, 'priya', '2022-01-07 12:31:16', '7757045213', '2022-01-07 12:31:16', '2022-01-07 12:31:26', 0, '43', 'ffd050121'),
(112, 'Ruchika bhardwaj', '2022-01-07 12:36:08', '7738537565', '2022-01-07 12:36:08', '2022-01-07 12:36:38', 1, '91', 'ffd050121'),
(113, 'jaikrishna singh', '2022-01-07 13:03:52', '09560140799', '2022-01-07 13:03:52', '2022-01-07 13:07:30', 1, '91', 'ffd050121'),
(114, 'Kusum Rawat', '2022-01-07 13:45:32', '7567869703', '2022-01-07 13:45:32', '2022-01-07 13:45:53', 0, '', 'ffd050121'),
(115, 'Sukhvinder  kaur', '2022-01-07 13:45:44', '9465262062', '2022-01-07 13:45:44', '2022-01-07 13:46:44', 1, '', 'ffd050121'),
(116, 'Raju s', '2022-01-07 13:51:11', '9845488904', '2022-01-07 13:51:11', '2022-01-07 13:52:43', 1, '', 'ffd050121');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
