<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {

$action = $_POST['action'];

switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }

            $start_from = ($page-1) * $limit;

            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Logged In Users: <?php 

                    $sql = "SELECT COUNT(id) as count FROM tbl_users where login_date != '0000-00-00 00:00:00' and batch='".$_SESSION['admin_batch']."'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
                    echo $total_records; 

                    ?>
                </div>
                <!--<div class="col-6">
                    Total Registered Users: <?php 

                    $sql = "SELECT COUNT(id) as count FROM tbl_users where batch='".$_SESSION['admin_batch']."'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_reg = $row['count'];  
                    echo $total_reg; 

                    ?>
                </div>-->
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Batch No</th>
                          <th>Phone No.</th>
                          <th>Login Time</th>
                          <th>Logout Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        //$query="select * from tbl_users where login_date != '0000-00-00 00:00:00' and batch='".$_SESSION['admin_batch']."' order by login_date desc LIMIT $start_from, $limit";
                        $query="select * from tbl_users where batch='".$_SESSION['admin_batch']."' order by login_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?></td>
                            <td><?php echo $data['batch']; ?></td>
                            <td><?php echo '+'.$data['cntry_code'] . ' - ' . $data['mobile_num']; ?></td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['logout_date'] != ''){
                                    $date=date_create($data['logout_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '<small>Never Logged in.<small>';
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getquestions':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) FROM tbl_questions where batch='".$_SESSION['admin_batch']."'";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            
            $count = $total_records - $start_from;
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Ques: <div id="ques_count"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6>"><div id="ques_update"></div></div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="30">#</th>  
                          <th width="200">Name</th>
                          <!-- <th width="200">Batch</th> -->
                          <th>Question</th>
                          <th width="200">Asked At</th>
                    
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_questions where batch='".$_SESSION['admin_batch']."' order by asked_at desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $i=$start_from;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <th><?= $count-- ?></th>
                            
                            <td><?php echo $data['user_name']; ?></td>
                            <!-- <td><?php echo $data['select_batch']; ?></td> -->
                            <td><?php echo $data['user_question']; ?>
                            <div class="actions">
                                <?php 
                                $resp = ''; $doctor = ''; $diet = ''; $exercise =''; $mentor='';$operations='';$InnerTransformation='';$Sales='';$Program=''; $remark='';
                                
                                if($data['responded']){ $resp = 'checked'; }
                                if($data['doctor']){ $doctor = 'checked'; }
                                if($data['diet']){ $diet = 'checked'; }
                                if($data['exercise']){ $exercise = 'checked'; }
                                if($data['mentor']){ $mentor = 'checked'; }
                                if($data['operations']){ $operations = 'checked'; }
                                if($data['InnerTransformation']){ $InnerTransformation = 'checked'; }
                                if($data['Sales']){ $Sales = 'checked'; }
                                if($data['Program']){ $Program = 'checked'; }
                                if($data['remark']){ $remark = 'checked'; }
                                
                                ?>
                                <input type="checkbox" <?php echo $resp; ?> class="form-check-inline" name="resp<?= $data['id'] ?>" id="resp<?= $data['id'] ?>" onClick="updResp('<?= $data['id'] ?>')"/>Responded &nbsp;
<input type="checkbox" <?php echo $doctor; ?> class="form-check-inline" name="doct<?= $data['id'] ?>" id="doct<?= $data['id'] ?>" onClick="updDoct('<?= $data['id'] ?>')"/>Doctor &nbsp;
 <input type="checkbox" <?php echo $diet; ?> class="form-check-inline" name="diet<?= $data['id'] ?>" id="diet<?= $data['id'] ?>" onClick="updDiet('<?= $data['id'] ?>')"/>Diet &nbsp;
 <input type="checkbox" <?php echo $exercise; ?> class="form-check-inline" name="exer<?= $data['id'] ?>" id="exer<?= $data['id'] ?>" onClick="updExer('<?= $data['id'] ?>')"/>Exercise &nbsp; 
 <input type="checkbox" <?php echo $mentor; ?> class="form-check-inline" name="ment<?= $data['id'] ?>" id="ment<?= $data['id'] ?>" onClick="updMent('<?= $data['id'] ?>')"/>Mentor &nbsp;
<input type="checkbox" <?php echo $operations; ?> class="form-check-inline" name="oper<?= $data['id'] ?>" id="oper<?= $data['id'] ?>" onClick="updOper('<?= $data['id'] ?>')"/>Operations &nbsp;
 <input type="checkbox" <?php echo $InnerTransformation;
                               ?>class="form-check-inline" name="inner<?= $data['id'] ?>" id="inner<?= $data['id'] ?>" onClick="updinner('<?= $data['id'] ?>')"/>InnerTransformation &nbsp;
                               <input type="checkbox" <?php echo $Sales; ?> class="form-check-inline" name="sales<?= $data['id'] ?>" id="sales<?= $data['id'] ?>" onClick="updsales('<?= $data['id'] ?>')"/>Sales &nbsp; 
                               <input type="checkbox" <?php echo $Program; ?> class="form-check-inline" name="Program<?= $data['id'] ?>" id="Program<?= $data['id'] ?>" onClick="updprogram('<?= $data['id'] ?>')"/>Program &nbsp;&nbsp;&nbsp;
                               Remarks &nbsp;&nbsp;&nbsp; <textarea  <?php echo $remark; ?>class="form-check-inline" name="remark<?= $data['id'] ?>" id="remark<?= $data['id'] ?>" onkeyup="updRemark('<?= $data['id'] ?>')"/ placeholder="Write something.." style="height:30px;width:400px"></textarea>
                               
                              
                            </div>
                            </td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>

                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        case 'logoutuser':
              $sql = "update tbl_users set logout_status='0' where id='".$_POST['userid']."' and batch='".$_SESSION['admin_batch']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        case 'getquesupdate':
              $sql = "SELECT COUNT(id) FROM tbl_questions where batch='".$_SESSION['admin_batch']."'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  

              echo $total_records;
        break;
        case 'updatespk':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set speaker ='$newval', answered='0' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  

              //echo $sql;
        break;
         case 'updatespkans':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set answered ='$newval' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  

              //echo $sql;
        break;
        case 'updatevalue':

              $option = $_POST['option'];
              $quesid = $_POST['quesid'];
              $val = $_POST['val'];

              $sql = "Update tbl_questions set ".$option." ='$val' where id = '".$quesid."'";  

              $rs_result = mysqli_query($link,$sql);
//$row = mysqli_fetch_row($rs_result);
//$total_records = $row[0];

echo $sql;
break;



}

}


?>