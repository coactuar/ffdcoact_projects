<?php
	require_once "config.php";
	
	if((!isset($_SESSION["user_phone"])) || (!isset($_SESSION["user_name"])))
	{
        session_destroy();
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone' and cntry_code='$code'  and batch='$batch'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            unset($_SESSION["user_id"]);
            unset($_SESSION["user_batch"]);
            /*if(isset($_SESSION["user_remember"]))
                  {
                      unset($_SESSION["user_remember"]);
                  }
            */
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="assets/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row mt-3 mb-2">
        <div class="col-9 col-md-3"> <a href="dashboard.php"><img src="img/logo.png" class="img-fluid img-logo" alt=""/></a> 
        </div>
        <div class="col-3 d-block d-md-none text-right">
        <ul class="list-inline ml-auto mb-0 top-nav">
                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        
                        <img src="img/profile.png" alt="Profile image" class="avatar-rounded">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-item">
                            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
                            <div class="batch">[<?php echo $_SESSION['user_batch']; ?>]</div>
                        </div>
            
                        <a href="?action=logout" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Logout</span>
                        </a>
                        
                    </div>
                </li>
            
            </ul>
        </div>
        <div class="col-12 col-md-6 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
        <div class="col-12 col-md-3 text-right d-none d-md-block">
            <ul class="list-inline ml-auto mb-0 top-nav">
                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        
                        <img src="img/profile.png" alt="Profile image" class="avatar-rounded">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-item">
                            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
                            <div class="batch">[<?php echo $_SESSION['user_batch']; ?>]</div>
                        </div>
            
                        <a href="?action=logout" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Logout</span>
                        </a>
                        
                    </div>
                </li>
            
            </ul> 
        </div>
        
    </div>
<div class="content-area">
    <div class="video-phases text-center">
    <?php
     $sql = "SELECT distinct tbl_videos.category, tbl_categories.category, tbl_categories.id FROM `tbl_batchvideos`, tbl_videos, tbl_categories where tbl_batchvideos.batch = '".$_SESSION['user_batch']."' and tbl_batchvideos.video_id = tbl_videos.id and tbl_videos.category = tbl_categories.id and tbl_videos.audio = '1'";
     //echo $sql;
     $res = mysqli_query($link, $sql) or die(mysqli_error($link));
     
     while($data = mysqli_fetch_assoc($res))
     {              
    ?>
          <a href="cataudios.php?c=<?php echo $data['id']; ?>"><button class="btn btn-outline-primary btn-lg"><?php echo $data['category']; ?></button></a>
      
    <?php
     }
    ?>
    </div>
    <div class="row mt-5 mb-2">
    <div class="col-12 text-center">
        <a href="dashboard.php"><button class="btn btn-outline-info btn-sm"><i class="fa fa-home"></i> Home</button></a>
    </div>
</div>
</div>

</div>



<nav class="navbar fixed-bottom bottom-nav">
  <div class="icons">
            <a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><i class="fas fa-2x fa-globe web"></i></a><a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-facebook-square fb"></i></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-youtube yt"></i></a><a href="https://www.instagram.com/freedomfromdiabetes/" target="_blank"><i class="fab fa-2x fa-instagram insta"></i></a>
            </div>
</nav>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function updateLogin()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='?action=logout';
			   }
         }
});
}
setInterval(function(){ updateLogin(); }, 30000);

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-17');
</script>

</body>
</html>