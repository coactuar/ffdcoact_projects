<?php
	require_once "config.php";
	
	if((!isset($_SESSION["user_phone"])) || (!isset($_SESSION["user_name"])))
	{
        session_destroy();
		header("location: index.php");
		exit;
	}
    $batch = $_SESSION['user_batch']; 
    $audid = $_GET['v'];
    $audcat = 0;
    $query="select active, category from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='$batch' and tbl_batchvideos.video_id='$audid' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1' and tbl_videos.audio='1'";
    $res = mysqli_query($link, $query) or die(mysqli_error($link));
    if (mysqli_affected_rows($link) > 0) 
    {
        $data = mysqli_fetch_assoc($res);
        $audcat = $data['category'];
    }
    else
    {
   		header("location: dashboard.php");
		exit;

    }
//echo $query;	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone' and cntry_code='$code'  and batch='$batch'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            unset($_SESSION["user_id"]);
            unset($_SESSION["user_batch"]);
            /*if(isset($_SESSION["user_remember"]))
                  {
                      unset($_SESSION["user_remember"]);
                  }
            */
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="assets/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/plyr.css">
</head>

<body>
<div class="container-fluid">
    <div class="row mt-3 mb-2">
        <div class="col-9 col-md-3"> <a href="dashboard.php"><img src="img/logo.png" class="img-fluid img-logo" alt=""/></a> 
        </div>
        <div class="col-3 d-block d-md-none text-right">
        <ul class="list-inline ml-auto mb-0 top-nav">
                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        
                        <img src="img/profile.png" alt="Profile image" class="avatar-rounded">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-item">
                            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
                            <div class="batch">[<?php echo $_SESSION['user_batch']; ?>]</div>
                        </div>
            
                        <a href="?action=logout" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Logout</span>
                        </a>
                        
                    </div>
                </li>
            
            </ul>
        </div>
        <div class="col-12 col-md-6 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
        <div class="col-12 col-md-3 text-right d-none d-md-block">
            <ul class="list-inline ml-auto mb-0 top-nav">
                <li class="list-inline-item dropdown notif">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        
                        <img src="img/profile.png" alt="Profile image" class="avatar-rounded">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-item">
                            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
                            <div class="batch">[<?php echo $_SESSION['user_batch']; ?>]</div>
                        </div>
            
                        <a href="?action=logout" class="dropdown-item notify-item">
                            <i class="fa fa-power-off"></i> <span>Logout</span>
                        </a>
                        
                    </div>
                </li>
            
            </ul> 
        </div>
        
    </div>
</div>

<div class="row" id="body-row">
    <!-- MAIN -->
    <div class="col content-area p-4">
        <div class="container-fluid">    
        <?php 
            $sql = "select * from tbl_videos where active = '1' and audio='1' and id='".$_GET['v']."'";
            $res = mysqli_query($link, $sql) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $aud_url = $data['video_url'];
            //echo $allow;
        ?>
    <div class="row mt-2">
        <div class="col-12 col-md-8 col-lg-7">
            <audio id="player" autoplay controls crossorigin playsinline></audio>
            <div class="meta">
              <h3><?php echo $data['video_title']; ?></h3>
              <h4><?php echo $data['video_desc']; ?></h4>
              <h5><?php 
                $view= "select count(*) as cnt from tbl_viewers where video_id='".$data['id']."'";
                $vres = mysqli_query($link, $view) or die(mysqli_error($link));
                $vdata = mysqli_fetch_assoc($vres);
                echo $vdata['cnt']; 
                //echo $view;
                ?> Views
                </h5>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-5 ">
            <?php 
                //$listsql = "select * from tblbatchvideos, tbl_videos where active = '1' and id != '' ORDER BY RAND() limit 3";
                $listsql="select * from tbl_batchvideos, tbl_videos where tbl_batchvideos.video_id !='".$_GET['v']."' and tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.audio='1' and tbl_videos.active='1' and tbl_videos.category='$audcat' ORDER BY RAND() limit 3";
                
                $listres = mysqli_query($link, $listsql) or die(mysqli_error($link));
                if(mysqli_affected_rows($link) > 0 )
                {
                while($listdata = mysqli_fetch_assoc($listres))
                {
                
            ?>
            <div class="row mb-2 p-2 border-bottom">
                <div class="col-2 col-md-2">
                    <a href="listen.php?v=<?php echo $listdata['id']; ?>"><i class="fa fa-headphones fa-2x"></i></a>
                </div>
                <div class="col-10 col-md-10">
                    <div class="meta">
                    <h3><a href="listen.php?v=<?php echo $listdata['id']; ?>"><?php echo $listdata['video_title']; ?></a></h3>
                    <h5><?php 
                   echo $listdata['views']; 
                //echo $view;
                ?> Views</h5> 
                    <?php if($listdata['video_date'] != '')
                    {
                          $listdate=date_create($listdata['video_date']);
                          echo ' | <h5>'.date_format($listdate,"M d, Y").'</h5>';
                    } ?>
                    </div>
                </div>
            </div>
            <?php
                }
                }
                else
                {
                ?>
                <div class="row mb-2 p-2 border-bottom">
                  <div class="col-10 offset-1 p-1 text-center">
                      <div class="meta">
                          <h5>No more audio available to listen right now.</h5>
                      </div>
                  </div>
                </div>
                
                <?php
                }
            ?>   
            <div class="row mt-5 mb-2">
                <div class="col-12 text-center">
                    <a href="cataudios.php?c=<?php echo $audcat; ?>"><button class="btn btn-outline-primary btn-sm">More Audio</button></a>
                </div>
            </div> 
        </div>
    </div>
    
    
</div>
        
    </div><!-- Main Col END -->
</div>



<nav class="navbar fixed-bottom bottom-nav">
  <div class="icons">
            <a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><i class="fas fa-2x fa-globe web"></i></a><a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-facebook-square fb"></i></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-youtube yt"></i></a><a href="https://www.instagram.com/freedomfromdiabetes/" target="_blank"><i class="fab fa-2x fa-instagram insta"></i></a>
            </div>
</nav>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdn.plyr.io/3.5.6/plyr.js"></script>
<script src="https://cdn.rawgit.com/video-dev/hls.js/18bb552/dist/hls.min.js"></script>
<script>
$(function(){
	const source = '<?php echo $aud_url; ?>';
	const audio = document.querySelector('audio');
	const player = new Plyr('#player', 
    {
        controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'airplay', 'fullscreen'],
        tooltips: { controls: true, seek: true },
        muted: false
    });
	
	if (!Hls.isSupported()) {
		audio.src = source;
	} else {
		const hls = new Hls();
		hls.loadSource(source);
		hls.attachMedia(audio);
		window.hls = hls;
	}
	window.player = player;
});

function updateView(viewid)
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'updateview', view: viewid},
         type: 'post',
         success: function(output) {
			   
         }
});
}
function updateLogin()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='?action=logout';
			   }
         }
});
}
setInterval(function(){ updateLogin(); }, 30000);
</script>
<?php 
    $start_date   = date('Y/m/d H:i:s');
    $end_date   = date('Y/m/d H:i:s', time() + 5);
    
    $sql = "insert into tbl_viewers(video_id, user_id, start_time, end_time) values('".$_GET['v']."','".$_SESSION['user_id']."','$start_date','$end_date')";
    //echo $sql;
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
    
    $last_id = mysqli_insert_id($link);
    echo "<script>setInterval(function(){ updateView('".$last_id."'); }, 5000);</script>" ; 
    
  
    $view= "select count(*) as cnt from tbl_viewers where video_id='".$data['id']."'";
    $vres = mysqli_query($link, $view) or die(mysqli_error($link));
    $vdata = mysqli_fetch_assoc($vres);
    $views = $vdata['cnt']; 
                
    $sql = "update tbl_videos set views ='$views' where id ='".$_GET['v']."'";
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-17');
</script>

</body>
</html>