<?php
require_once "config.php";

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Audio AWS</title>
<link rel="stylesheet" type="text/css" href="css/plyr.css">

<style>
</style>

</head>

<body>
<audio id="player" autoplay controls crossorigin playsinline></audio>

<script src="js/jquery.min.js"></script>
<script src="https://cdn.plyr.io/3.5.6/plyr.js"></script>
<script src="https://cdn.rawgit.com/video-dev/hls.js/18bb552/dist/hls.min.js"></script>
<script>
$(function(){
	const source = 'https://d28fp6yvlbvtrp.cloudfront.net/out/v1/9bf022b16bce40ba8d594a39f9f0622a/5f32b7c297254580aa11f03372ac8a5b/9bed8f60e7d642fc98fe43a4da5f84bc/index.m3u8';
	const audio = document.querySelector('audio');
	
	// For more options see: https://github.com/sampotts/plyr/#options
	// captions.update is required for captions to work with hls.js
	const player = new Plyr('#player', 
    {
        controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'settings', 'airplay', 'fullscreen'],
        //settings: ['captions', 'quality', 'speed', 'loop'],
        //settings: ['quality', 'speed'],
        tooltips: { controls: true, seek: true },
        //speed:	{ selected: 1, options: [0.5, 1,  1.5,  2] },
        //quality: { default: 360, options: [720, 360, 160] },
        muted: false
    });
	
	if (!Hls.isSupported()) {
		audio.src = source;
	} else {
		// For more Hls.js options, see https://github.com/dailymotion/hls.js
		const hls = new Hls();
		hls.loadSource(source);
		hls.attachMedia(audio);
		window.hls = hls;
	}
	
	// Expose player so it can be used from the console
	window.player = player;
    
});

</script>    
</div>
</body>
</html>