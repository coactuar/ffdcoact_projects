<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_users where active = '0'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Phone No.</th>
                          <th>Batch</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users where active = '0' order by id desc LIMIT $start_from, $rec_limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?></td>
                            <td><?php echo '+'.$data['cntry_code'] .'-'.$data['mobile_num']; ?></td>
                            <td><?php echo $data['batch']; ?></td>
                            <td><a href="#" onClick="allow('<?php echo $data['id']; ?>')" class="btn btn-info btn-sm">Allow</a> <a href="#" onClick="deleteuser('<?php echo $data['id']; ?>')" class="btn btn-danger btn-sm">Delete</a></td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'allowuser':
              $sql = "update tbl_users set active='1' where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'deleteuser':
              $sql = "delete from tbl_users where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;

        
    }
    
}

?>