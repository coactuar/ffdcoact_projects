<?php
require_once "../config.php";

    $title     = mysqli_real_escape_string($link, $_POST['vidtitle']);
    $desc     = mysqli_real_escape_string($link, $_POST['viddesc']);
    $date     = mysqli_real_escape_string($link, $_POST['viddate']);
    $url     = mysqli_real_escape_string($link, $_POST['vidurl']);
    $active     = mysqli_real_escape_string($link, $_POST['active']);
    $cat     = mysqli_real_escape_string($link, $_POST['category']);
    $id     = mysqli_real_escape_string($link, $_POST['vidid']);
    $curr_thumb     = mysqli_real_escape_string($link, $_POST['vidthumb']);
    
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
    
    $img = $curr_thumb;
    $msg = '';
    $error = 0;
    
    if ($_FILES['thumbnail']['error'] > UPLOAD_ERR_OK)
    {
        $sql = "delete from tbl_batchvideos where video_id='$id'";
        $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
        if(isset($_POST['chklist']))
        {        
        foreach($_POST['chklist'] as $val)
        {
            //$desc .= $val;
            $sql = "insert into tbl_batchvideos(video_id, batch) values('$id','$val')";
            $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
        }
        }
                //echo $desc;
        $query="update tbl_videos set video_title='$title', video_desc='$desc', video_date='$date', video_url='$url', category='$cat', active='$active' where id ='$id'";
        //echo $query;
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        
        echo "s";

    }
    else
    {
        //$img = $_FILES['thumbnail']['name'];
        $tmp = $_FILES['thumbnail']['tmp_name'];
        
        $target_dir = "../img/thumbs/";
        $thumb = $_FILES["thumbnail"]["name"];
        $thumb = str_replace(' ','',$thumb);
        $thumb = strtolower($thumb);
        $target_file = $target_dir . basename($thumb);
        $ext = strtolower(pathinfo($thumb, PATHINFO_EXTENSION));
        
        if($_FILES['thumbnail']['size'] > 1000000) {
          $msg = "Image size should not be greated than 1Mb";
          $error = 1;
        }
        
        if(!in_array($ext, $valid_extensions)) 
        {
            $msg = "Only images are allowed";
            $error = 1;   
        }
        if(!$error)
        {
            $final_image = strtolower($target_file);
            $path = strtolower($final_image);
            //echo $path;
            if(move_uploaded_file($tmp,$path)) 
            {
                $sql = "delete from tbl_batchvideos where video_id='$id'";
                $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
                
                if(isset($_POST['chklist']))
                {
                foreach($_POST['chklist'] as $val)
                {
                    //$desc .= $val;
                    $sql = "insert into tbl_batchvideos(video_id, batch) values('$id','$val')";
                    $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
                }
                }
                //success
                $query="update tbl_videos set video_title='$title', video_desc='$desc', video_date='$date', video_url='$url', category='$cat', thumbnail_url='$thumb', active='$active' where id ='$id'";
                //echo $query;
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
        
                echo "s";
            }
            else
            {
                echo "Image Upload failed";
            }

        }
        else
        {
            echo $msg;
        }
    }
    
    //$query="update tbl_videos set video_title='$title', video_desc='$desc', video_date='$date', video_url='$url', category='$cat', active='$active' where id ='$id'";
    //echo $query;
    //$res = mysqli_query($link, $query) or die(mysqli_error($link));
    
    //echo "s";

?>