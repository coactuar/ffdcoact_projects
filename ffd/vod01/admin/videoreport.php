<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Video Report</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="videos.php">Videos</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 p-3">
        <div class="col-12">
            <div id="videoreport"> </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
getVidReport('1');
});

function update(pageNum)
{
  getVidReport(pageNum);
}

function getVidReport(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getvideoreport', page: pageNum, vidid: <?php echo $_GET['v']; ?>},
        type: 'post',
        success: function(response) {
            
            $("#videoreport").html(response);
            
        }
    });
    
}

</script>

</body>
</html>