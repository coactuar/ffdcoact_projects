<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_users where login_date != ''";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Phone No.</th>
                          <th>Last Login Time</th>
                          <th>Batch</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users where login_date != '' order by login_date desc LIMIT $start_from, $rec_limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?></td>
                            <td><?php echo '+'.$data['cntry_code'] .'-'.$data['mobile_num']; ?></td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php echo $data['batch']; ?></td>
                            <td><a href="userreport.php?u=<?php echo $data['id']; ?>" class="btn btn-success btn-sm">Watch Viewing History</a></td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getvideos':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_videos";  
            
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Videos: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th></th>
                          <th>Title</th>
                          <th>Views</th>
                          <th></th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_videos order by active desc LIMIT $start_from, $rec_limit";
                        //echo $query;
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td width="150"><img src="../img/thumbs/<?php echo $data['thumbnail_url']; ?>" class="video-thumb"  alt=""/></td>
                            <td><?php echo $data['video_title'] ?></td>
                            <td width="100"><?php echo $data['views'] ?></td>
                            <td width="300">
                            <a href="editvideo.php?v=<?php echo $data['id']; ?>" class="btn btn-info btn-sm">Edit</a>
                            <a href="#" onClick="delvideo('<?php echo $data['id']; ?>')" class="btn btn-info btn-sm btn-danger">Delete</a>
                            <a href="videoreport.php?v=<?php echo $data['id']; ?>" class="btn btn-success btn-sm">Watch Report</a>
                            </td>
                            <td width="50"><?php 
                                if($data['active'] == '1'){
                                    echo 'A'; 
                                }
                                else
                                {
                                    echo 'NA';
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'logoutuser':
              $sql = "update tbl_users set logout_status='0' where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'delvideo':
              $sql = "delete from tbl_batchvideos where video_id='".$_POST['vidid']."'";  
              $rs_result = mysqli_query($link,$sql);  

              $sql = "delete from tbl_viewers where video_id='".$_POST['vidid']."'";  
              $rs_result = mysqli_query($link,$sql);  


              $sql = "delete from tbl_videos where id='".$_POST['vidid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              
        break;
        
        
        case 'getcategories':
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_categories";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row cat mb-3">
                      <?php		
                        $query="select * from tbl_categories LIMIT $start_from, $rec_limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                         <div class="col-6 col-md-4 cat">
                            <?php echo $data['category']; ?>
                         </div> 
                      <?php			
                        }
                      ?>
                  
                     
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        break;
        
        case 'getvideoreport':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
            
            $vid = $_POST['vidid'];
            $sql = "SELECT * FROM tbl_videos where id ='$vid'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $title = $row['video_title'];  
            
        
            $sql = "SELECT COUNT(id) as count FROM tbl_viewers where video_id ='$vid'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row">
                <div class="col-9">
                    <?php echo $title; ?>
                </div>
                <div class="col-3 txt-right">
                    Total Views: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th></th>
                          <th>Viewed By</th>
                          <th>Phone No.</th>
                          <th>Batch</th>
                          <th>Start Time</th>
                          <th>End Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_viewers, tbl_users where video_id ='$vid' and tbl_viewers.user_id=tbl_users.id order by start_time desc LIMIT $start_from, $rec_limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        //echo $query;
                        $i = $total_records;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td width="10"><?php //echo $i; ?></td>
                            <td><?php echo $data['name']; ?></td>
                            <td>
                            <?php echo '+'.$data['cntry_code'] .'-'.$data['mobile_num']; ?></td>
                            <td><?php echo $data['batch']; ?></td>
                            <td width="225"><?php 
                                $date=date_create($data['start_time']);
                                    echo date_format($date,"M d, H:i:s a"); 
                                ?>
                            </td>
                            <td width="225"><?php 
                                $today=date("Y/m/d H:i:s");
                                $dateTimestamp1 = strtotime($data['end_time']);
                                $dateTimestamp2 = strtotime($today);
                                
                                if ($dateTimestamp1 > $dateTimestamp2)
                                {
                                  echo "Still Watching!";
                                }
                                else
                                {
                                $date=date_create($data['end_time']);
                                echo date_format($date,"M d, H:i:s a"); 
                                }
                                ?>
                            
                            </td>
                          </tr>
                      <?php	
                      $i--;		
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getuserreport':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $rec_limit;
            
            $uid = $_POST['userid'];
        
            $sql = "SELECT COUNT(id) as count FROM tbl_viewers where user_id ='$uid'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $rec_limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Videos Watched: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th></th>
                          <th>Video Watched</th>
                          <th>Start Time</th>
                          <th>End Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_viewers, tbl_videos where user_id ='$uid' and tbl_viewers.video_id=tbl_videos.id order by start_time desc LIMIT $start_from, $rec_limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        //echo $query;
                        $i = $total_records;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td width="10"><?php //echo $i; ?></td>
                            <td><?php echo $data['video_title']; ?></td>
                            <td><?php 
                                $date=date_create($data['start_time']);
                                    echo date_format($date,"M d, H:i:s a"); 
                                ?>
                            </td>
                            <td><?php 
                                $today=date("Y/m/d H:i:s");
                                $dateTimestamp1 = strtotime($data['end_time']);
                                $dateTimestamp2 = strtotime($today);
                                
                                if ($dateTimestamp1 > $dateTimestamp2)
                                {
                                  echo "Still Watching!";
                                }
                                else
                                {
                                $date=date_create($data['end_time']);
                                echo date_format($date,"M d, H:i:s a"); 
                                }
                                ?>
                            
                            </td>
                          </tr>
                      <?php	
                      $i--;		
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
    }
    
}

?>