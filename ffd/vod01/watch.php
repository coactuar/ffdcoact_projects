<?php
	require_once "config.php";
	
	if((!isset($_SESSION["user_phone"])) || (!isset($_SESSION["user_name"])))
	{
		header("location: index.php");
		exit;
	}
    $batch = $_SESSION['user_batch']; 
    $vidid = $_GET['v'];
    $query="select active from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='$batch' and tbl_batchvideos.video_id='$vidid' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1'";
    $res = mysqli_query($link, $query) or die(mysqli_error($link));
    if (mysqli_affected_rows($link) > 0) 
    {
    }
    else
    {
   		header("location: dashboard.php");
		exit;

    }
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            if(isset($_SESSION["user_remember"]))
                  {
                      unset($_SESSION["user_remember"]);
                  }
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="assets/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <div class="row mt-3 mb-2">
        <div class="col-12 col-md-3"> <a href="dashboard.php"><img src="img/logo.png" class="img-fluid img-logo" alt=""/></a> 
        </div>
        <div class="col-12 col-md-6 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
        <div class="col-12">
        <div class="menu-nav dropdown d-sm-block d-md-none">
           <button type="button" class="btn btn-nav btn-sm btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Menu
          </button> 
          
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="dashboard.php">Home</a>
            <div class="dropdown-divider"></div>
            <?php		
              $query="select * from tbl_categories";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
               <a class="dropdown-item" href="catvideos.php?c=<?php echo $data['id']; ?>"><?php echo $data['category']; ?></a> 
            <?php			
              }
            ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="?action=logout">Logout</a>
          </div>
        </div>
        </div>
    </div>
</div>

<div class="row" id="body-row">
    <!-- Sidebar -->
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
        <div class="user-det">
            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
            <div class="batch"><?php echo $_SESSION['user_batch']; ?></div>
        </div>
        <!-- Bootstrap List Group -->
        <ul class="list-group">
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>MAIN MENU</small>
            </li>
            <a href="dashboard.php" class="bg-info list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed">Home</span>
                </div>
            </a>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>CATEGORIES</small>
            </li>
            <!-- /END Separator -->
            <?php		
              $query="select * from tbl_categories";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
               <a href="catvideos.php?c=<?php echo $data['id']; ?>" class="bg-info list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fa fa-th-list fa-fw mr-3"></span>
                        <span class="menu-collapsed"><?php echo $data['category']; ?></span>
                    </div>
                </a> 
            <?php			
              }
            ?>
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>OPTIONS</small>
            </li>
            <a href="?action=logout" class="bg-info list-group-item list-group-item-action logout">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed">Logout</span>
                </div>
            </a>
        </ul><!-- List Group END-->
    </div><!-- sidebar-container END -->
    <!-- MAIN -->
    <div class="col content-area p-4">
        <div class="container-fluid">    
    
    <div class="row">
        <div class="col-12 text-center">
        <?php 
            $sql = "select * from tbl_videos where active = '1' and id='".$_GET['v']."'";
            $res = mysqli_query($link, $sql) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            
            //echo $allow;
        ?>
        
        </div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-10 col-lg-9">
            <div id="player" class="video-player"></div>
            <div class="meta">
              <h3><?php echo $data['video_title']; ?></h3>
              <h4><?php echo $data['video_desc']; ?></h4>
              <h5><?php 
                $view= "select count(*) as cnt from tbl_viewers where video_id='".$data['id']."'";
                $vres = mysqli_query($link, $view) or die(mysqli_error($link));
                $vdata = mysqli_fetch_assoc($vres);
                echo $vdata['cnt']; 
                //echo $view;
                ?> Views
                </h5>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <?php 
                //$listsql = "select * from tblbatchvideos, tbl_videos where active = '1' and id != '' ORDER BY RAND() limit 3";
                $listsql="select * from tbl_batchvideos, tbl_videos where tbl_batchvideos.video_id !='".$_GET['v']."' and tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1' ORDER BY RAND() limit 3";
                
                $listres = mysqli_query($link, $listsql) or die(mysqli_error($link));
                while($listdata = mysqli_fetch_assoc($listres))
                {
                
            ?>
            <div class="row mt-1">
                <div class="col-10">
                    <a href="watch.php?v=<?php echo $listdata['id']; ?>"><img src="img/thumbs/<?php echo $listdata['thumbnail_url']; ?>" class="img-fluid"  alt=""/> </a>
                </div>
            </div>
            <div class="row video-list-desc mb-1">
                <div class="col-10">
                    <div class="meta">
                    <h3><a href="watch.php?v=<?php echo $listdata['id']; ?>"><?php echo $listdata['video_title']; ?></a></h3>
                    <h5><?php 
                   echo $listdata['views']; 
                //echo $view;
                ?> Views</h5> 
                    <?php if($listdata['video_date'] != '')
                    {
                          $listdate=date_create($listdata['video_date']);
                          echo ' | <h5>'.date_format($listdate,"M d, Y").'</h5>';
                    } ?>
                    </div>
                </div>
            </div>
            <?php
                }
            ?>    
        </div>
    </div>
    
    
</div>
        
    </div><!-- Main Col END -->
</div>



<nav class="navbar fixed-bottom bottom-nav">
  <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
</nav>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    var player = new Clappr.Player(
    {
        source:<?php echo '"'.$data['video_url'].'"'; ?>,
        parentId: "#player",
        poster: "img/thumbs/<?php echo $data['thumbnail_url']; ?>",
        width: "100%",
        height: "100%",
        
    });
    
    player.play();
function updateView(viewid)
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'updateview', view: viewid},
         type: 'post',
         success: function(output) {
			   
         }
});
}
function updateLogin()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ updateLogin(); }, 30000);
</script>
<?php 
    $start_date   = date('Y/m/d H:i:s');
    $end_date   = date('Y/m/d H:i:s', time() + 5);
    
    $sql = "insert into tbl_viewers(video_id, user_id, start_time, end_time) values('".$_GET['v']."','".$_SESSION['user_id']."','$start_date','$end_date')";
    //echo $sql;
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
    
    $last_id = mysqli_insert_id($link);
    echo "<script>setInterval(function(){ updateView('".$last_id."'); }, 5000);</script>" ; 
    
  
    $view= "select count(*) as cnt from tbl_viewers where video_id='".$data['id']."'";
    $vres = mysqli_query($link, $view) or die(mysqli_error($link));
    $vdata = mysqli_fetch_assoc($vres);
    $views = $vdata['cnt']; 
                
    $sql = "update tbl_videos set views ='$views' where id ='".$_GET['v']."'";
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-17');
</script>

</body>
</html>