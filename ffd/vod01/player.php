<?php
require_once "config.php";

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<?php 
    $sql = "select video_url, thumbnail_url, views from tbl_videos where active = '1' and id='".$_GET['v']."'";
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
    $data = mysqli_fetch_assoc($res);
    $views = $data['views'];
    
?>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source:"https://wqexygpkzslw4p.data.mediastore.ap-northeast-1.amazonaws.com/abb/live.m3u8",
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              1: '360p', // 500kbps
              0: '180p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        poster: "img/thumbs/<?php echo $data['thumbnail_url']; ?>",
        width: "100%",
        height: "100%",
        
    });
    
    player.play();
</script>
<?php 
    $views = $data['views'] + 1;
    $sql = "update tbl_videos set views='$views' where id='".$_GET['v']."'";
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));

?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>
</body>
</html>