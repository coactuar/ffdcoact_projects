<?php
	require_once "config.php";
	
	if((!isset($_SESSION["user_phone"])) || (!isset($_SESSION["user_name"])))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            if(isset($_SESSION["user_remember"]))
                  {
                      unset($_SESSION["user_remember"]);
                  }
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="assets/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row mt-3 mb-2">
        <div class="col-12 col-md-3"> <a href="dashboard.php"><img src="img/logo.png" class="img-fluid img-logo" alt=""/></a> 
        </div>
        <div class="col-12 col-md-6 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
        <div class="col-12">
        <div class="menu-nav dropdown d-sm-block d-md-none">
           <button type="button" class="btn btn-nav btn-sm btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Menu
          </button> 
          
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="dashboard.php">Home</a>
            <div class="dropdown-divider"></div>
            <?php		
              $query="select * from tbl_categories";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
               <a class="dropdown-item <?php if($_GET['c'] == $data['id']) echo 'active'; ?>" href="catvideos.php?c=<?php echo $data['id']; ?>"><?php echo $data['category']; ?></a> 
            <?php			
              }
            ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="?action=logout">Logout</a>
          </div>
        </div>
        </div>
    </div>
</div>
<!-- NavBar END -->
<div class="row" id="body-row">
    <!-- Sidebar -->
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
        <div class="user-det">
            <div class="name"><?php echo $_SESSION['user_name']; ?></div>
            <div class="batch"><?php echo $_SESSION['user_batch']; ?></div>
        </div>
        <!-- Bootstrap List Group -->
        <ul class="list-group">
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>MAIN MENU</small>
            </li>
            <a href="dashboard.php" class="bg-info list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed">Home</span>
                </div>
            </a>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>CATEGORIES</small>
            </li>
            <!-- /END Separator -->
            <?php		
              $query="select * from tbl_categories";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while($data = mysqli_fetch_assoc($res))
              {
              ?>
               <a href="catvideos.php?c=<?php echo $data['id']; ?>" class="bg-info list-group-item list-group-item-action <?php if($_GET['c'] == $data['id']) echo 'active'; ?> ">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fa fa-th-list fa-fw mr-3"></span>
                        <span class="menu-collapsed"><?php echo $data['category']; ?></span>
                    </div>
                </a> 
            <?php			
              }
            ?>
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>OPTIONS</small>
            </li>
            <a href="?action=logout" class="bg-info list-group-item list-group-item-action logout">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out-alt fa-fw mr-3"></span>
                    <span class="menu-collapsed">Logout</span>
                </div>
            </a>
        </ul><!-- List Group END-->
    </div><!-- sidebar-container END -->
    <!-- MAIN -->
    <div class="col content-area p-4">
        <div class="container-fluid">    
          <form class="form-inline search">
            <div class="row">
              <div class="col-12 col-md-8 offset-md-1 text-center mb-2">
                  <input type="text" class="form-control" id="key" name="key"> 
              </div>
              <div class="col-12 col-md-1 text-center">
                  <button type="button"  id="search" class="btn btn-search btn-default">Search</button>
              </div>
              
            </div>
            
          </form>
      
          <div id="cat-videos"></div>
      </div>
        
    </div><!-- Main Col END -->
</div><!-- body-row END -->




<nav class="navbar fixed-bottom bottom-nav">
  <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
</nav>

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

<script>
function updateLogin()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ updateLogin(); }, 30000);

$(function(){
    getCatVids('1', '');
});

function update(pageNum, keyword)
{
  getCatVids(pageNum, keyword);
}

function getCatVids(pageNum, keyword)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getcatvids', page: pageNum, keyword: keyword, catid: <?php echo $_GET['c']; ?>},
        type: 'post',
        success: function(response) {
            $("#cat-videos").html(response);
        }
    });
    
}
$(function(){
     
	$("#search").click(function() 
    {  
        var keyword = $('#key').val();
        getCatVids('1', keyword);     
        return false;
    });
});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-17');
</script>

</body>
</html>