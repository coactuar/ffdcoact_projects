<?php
require_once "config.php";
if(!isset($_SESSION["user_phone"]))
	{
		header("location: index.php");
		exit;
	}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<?php 
    $sql = "select video_url from tbl_videos where active = '1' and id='".$_GET['v']."'";
    $res = mysqli_query($link, $sql) or die(mysqli_error($link));
    $data = mysqli_fetch_assoc($res);
    
?>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source:<?php echo '"'.$data['video_url'].'"'; ?>,
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              1: '360p', // 500kbps
              0: '180p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        //poster: "img/poster.jpg",
        width: "100%",
        height: "100%",
        
    });
    
    player.play();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>
</body>
</html>