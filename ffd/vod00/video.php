<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_phone"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row mt-3 mb-2 top-img">
        <div class="col-4 col-md-3 text-center ">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-8 col-md-6 text-center ">
            <img src="img/top-heading.png" class="img-fluid" alt=""/> 
        </div>
    </div>
</div>
<div class="container">    
    <div class="row">
        <div class="col-12 text-right">
            Hello! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
        <?php 
            $sql = "select * from tbl_videos where active = '1' and id='".$_GET['v']."'";
            $res = mysqli_query($link, $sql) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
        ?>
        <h3 class="title"><?php echo $data['video_title']; ?></h3>
            <div class="desc">
            <?php echo $data['video_desc']; ?>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2 text-center">
            <div class="embed-responsive embed-responsive-16by9 video-panel mt-2">
                  <iframe class="embed-responsive-item" id="webcast" src="vod.php?v=<?php echo $_GET['v']; ?>" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 text-center">
            <img src="img/back.png" class="back" height="25" alt=""/>&nbsp;&nbsp;<a href="webcast.php" class="list">List of Videos</a>
        </div>
    </div>
    
</div>
<div class="container-fluid">    
    <div class="row mt-3 mb-1">
        <div class="col-12 text-center">
            <div class="icons bg-black">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>



<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>