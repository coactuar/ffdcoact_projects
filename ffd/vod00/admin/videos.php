<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Videos</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<div class="container-fluid">
    <div class="row mt-3 mb-2 top-img">
        <div class="col-4 col-md-3 text-center ">
            <img src="../img/logo.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-8 col-md-6 text-center ">
            <img src="../img/top-heading.png" class="img-fluid" alt=""/> 
        </div>
    </div>
</div>

<div class="container main">
    
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="videos.php">Videos</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>    
</div>    
<div class="container">
    <div class="row mt-1">
        <div class="col-12">
        <a href="addvideo.php" class="btn btn-success"> Add Video </a></div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="videos"> </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
getVids('1');
});

function update(pageNum)
{
  getVids(pageNum);
}

function getVids(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getvideos', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#videos").html(response);
            
        }
    });
    
}

</script>

</body>
</html>