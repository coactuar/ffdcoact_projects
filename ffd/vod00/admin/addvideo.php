<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Videos</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<div class="container-fluid">
    <div class="row mt-3 mb-2 top-img">
        <div class="col-4 col-md-3 text-center ">
            <img src="../img/logo.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-8 col-md-6 text-center ">
            <img src="../img/top-heading.png" class="img-fluid" alt=""/> 
        </div>
    </div>
</div>

<div class="container main">
    
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="videos.php">Videos</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>    
</div>    
<div class="container">
    
    <div class="row mt-1">
<div class="col-12 col-md-8 offset-md-2 text-center">
            <form id="addvideo-form" method="post">
            <h1>Add Video</h1>
              <div id="login-message"></div>
              <div class="input-group mt-1 mb-1">
                <input type="text" class="form-control" placeholder="Video Title" aria-label="Video Title" aria-describedby="basic-addon1" name="vidtitle" id="vidtitle" required>
              </div>
              <div class="input-group mt-1 mb-1">
                <textarea class="form-control" placeholder="Video Description" aria-label="Video Desc." aria-describedby="basic-addon1" name="viddesc" id="viddesc" rows="4"></textarea>
              </div>
              <div class="input-group mt-1 mb-1">
                <input type="date" class="form-control" placeholder="Video Date" aria-label="Video Date" aria-describedby="basic-addon1" name="viddate" id="viddate">
              </div>
              <div class="input-group mt-1 mb-1">
                <textarea class="form-control" placeholder="Video URL" aria-label="Video URL" aria-describedby="basic-addon1" name="vidurl" id="vidurl" rows="2" required></textarea>
              </div>
              <div class="input-group mt-1 mb-1">
                <select id="active" name="active" class="form-control" required>
                     <option value="1">Active</option>
                     <option value="0">Not Active</option>
                </select>
              </div>
              <div class="input-group mt-1 mb-1">
                <button class="mt-4 btn btn-block" type="submit">Add Video</button>
              </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#addvideo-form', function()
{  

    
  $.post('add_video.php', $(this).serialize(), function(data)
  {
      
      if(data =='s')
      {
        window.location = 'videos.php';   
      }
      
  });
  
  return false;
});

});

</script>

</body>
</html>