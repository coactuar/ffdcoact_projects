<?php
	require_once "../config.php";
	
	if((!isset($_SESSION["admin_user"])) || (!isset($_SESSION['admin_batch'])) )
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            unset($_SESSION["admin_batch"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Users</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light top-nav">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>
</nav>

<div class="container-fluid">
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="questions.php">Questions</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-4">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
        <div class="col-4">
        <!-- <select id="batch" name="batch" class="form-control" required onchange="getUsers1();">
                    <option value="0">Select Batch</option>

                    
                     <option value="batch54">Intensive Batch 54
                     </option>
                    <option value="batch55">Intensive Batch 55</option>
                    <option value="batch56">Intensive Batch 56</option>
                    <option value="batch57">Intensive Batch 57 </option>
                    <option>
             <?php 
            $batch = str_split($_SESSION['admin_batch'],5);
            echo 'Batch54 ' . $batch[1]; 
            ?> 
            </option>    

                </select> -->
       

        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="users"> </div>
        </div>
    </div>
</div>
<div class="row mt-1">
        <div class="col-12">
            <div id="users1"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>


// function showCustomer() {
//     $.ajax({
// 						type: 'GET',
// 						url: 'display.php',
// 						data: {action: 'updatevalue',users1:batch},
// 						success: function(data){
// 						console.log(data);
// 							$("#users1").html(data);
// 						}
// 					});
                    
//                     }


function getUsers1(pageNum)
{
    $.ajax({
        url: 'display.php',
        data: {action: 'getusers', user1: batch},
        type: 'post',
        success: function(response) {
            
            $("#users1").html(response);
            
        }
    });
    
}


$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}

function logoutUser(uid)
{
   $.ajax({
        url: 'ajax.php',
         data: {action: 'logoutuser', userid: uid},
         type: 'post',
         success: function(output) {
             getUsers('1');
         }
   });
}
</script>

</body>
</html>